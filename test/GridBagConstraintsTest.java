import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;


/**
 * @author XiongYingqi
 * @version 2013-7-1 下午5:28:11
 */
public class GridBagConstraintsTest extends JFrame{
	private final JLabel nicknameLabel = new JLabel();
	private final JTextArea statusLabel = new JTextArea();
	private final JLabel fullJIDLabel = new JLabel();
	private final JLabel avatarLabel = new JLabel();
	private final JLabel iconLabel = new JLabel();
	private final JLabel titleLabel = new JLabel();
	private JPanel mainPanel;
	private JLabel label1;

	private JPanel getJPanel0() {
		if (mainPanel == null) {
			mainPanel = new JPanel();
			mainPanel.setLayout(new GroupLayout());
		}
		return mainPanel;
	}

	private void initComponents() {
		add(getMainPanel(), BorderLayout.CENTER);
		
		mainPanel.add(avatarLabel, new GridBagConstraints(0, 1, 1, 3, 0.0, 0.0,
				GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
				new Insets(2, 2, 2, 2), 0, 0));
		mainPanel.add(iconLabel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.NONE,
				new Insets(2, 2, 0, 2), 0, 0));
		mainPanel.add(nicknameLabel, new GridBagConstraints(2, 1, 1, 1, 1.0, 0.0,
				GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
				new Insets(2, 0, 0, 2), 0, 0));
		mainPanel.add(statusLabel,
				new GridBagConstraints(2, 2, 1, 1, 1.0, 0.0,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0,
								2), 0, 0));
		mainPanel.add(titleLabel,
				new GridBagConstraints(2, 3, 1, 1, 1.0, 0.0,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 0, 2,
								2), 0, 0));
		mainPanel.add(fullJIDLabel,
				new GridBagConstraints(0, 5, 4, 1, 1.0, 1.0,
						GridBagConstraints.SOUTHWEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 2, 2,
								2), 0, 0));
		setSize(320, 240);
	}

	private JPanel getMainPanel() {
		if (mainPanel == null) {
			mainPanel = new JPanel();
			mainPanel.setBackground(Color.white);
			mainPanel.setLayout(new GroupLayout());
			mainPanel.add(getJLabel0(), new Constraints(new Leading(20, 10, 10), new Leading(22, 10, 10)));
		}
		return mainPanel;
	}

	private JLabel getJLabel0() {
		if (label1 == null) {
			label1 = new JLabel();
			label1.setText("label1");
		}
		return label1;
	}

	public GridBagConstraintsTest() {
		initComponents();
	}
	
	public static void main(String[] args) {
		GridBagConstraintsTest test = new GridBagConstraintsTest();
		test.setVisible(true);
	}


}
