/**
 * spark_src
 */
package com.xiongyingqi.xmpp;

import org.jivesoftware.Spark;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Session;
import org.jivesoftware.spark.SparkManager;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-22 下午3:40:59
 */
public class XmppConnection {
	private XMPPConnection connection;
	public XmppConnection(){
		new Spark().startup();
		connection = new XMPPConnection("127.0.0.1");
		Connection.DEBUG_ENABLED = true;
		try {
			connection.connect();
			connection.login("熊瑛琪", "111", "test");
		} catch (XMPPException e) {
			e.printStackTrace();
		}
	}
	/**
	 * XMPPConnection
	 * @return the connection
	 */
	public XMPPConnection getConnection() {
		return connection;
	}
	/**
	 * XMPPConnection
	 * @param connection the connection to set
	 */
	public void setConnection(XMPPConnection connection) {
		this.connection = connection;
	}
	
}
