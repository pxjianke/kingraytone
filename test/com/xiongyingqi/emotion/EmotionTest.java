/**
 * spark_src
 */
package com.xiongyingqi.emotion;

import java.io.File;

import org.jivesoftware.smack.provider.ProviderManager;
import org.junit.Test;

import com.kingray.spark.packet.KingrayNameSpace;
import com.xiongyingqi.xmpp.XmppConnection;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-22 下午3:40:59
 */
public class EmotionTest extends XmppConnection{
	
	@Test
	public void testAddEmotion(){
		EmotionService.getInstance().setConnection(super.getConnection());
		ProviderManager.getInstance().addIQProvider(KingrayNameSpace.QUERY_EMOTION.getKey(), KingrayNameSpace.QUERY_EMOTION.getValue(), new EmotionIQProvider());
		super.getConnection().addPacketListener(EmotionService.getInstance(), EmotionService.getInstance());
		EmotionManager.getInstance().addEmotion(new File("D:\\MyWork\\files\\素材\\gif\\qq"), "", false);
	}
	public static void main(String[] args) {
		EmotionTest test = new EmotionTest();
		test.testAddEmotion();
	}
}
