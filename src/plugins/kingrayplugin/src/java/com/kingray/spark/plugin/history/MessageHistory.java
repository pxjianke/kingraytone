/**
 * spark_src
 */
package com.kingray.spark.plugin.history;

import org.jivesoftware.spark.util.log.Log;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.text.html.HTMLEditorKit;

import org.jdesktop.swingx.calendar.DateUtils;
import org.jivesoftware.MainWindowListener;
import org.jivesoftware.resource.Res;
import org.jivesoftware.resource.SparkRes;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.spark.component.BackgroundPanel;
import org.jivesoftware.spark.plugin.ContextMenuListener;
import org.jivesoftware.spark.ui.ChatRoom;
import org.jivesoftware.spark.ui.ChatRoomButton;
import org.jivesoftware.spark.ui.ChatRoomClosingListener;
import org.jivesoftware.spark.ui.ChatRoomListener;
import org.jivesoftware.spark.ui.ContactItem;
import org.jivesoftware.spark.ui.ContactList;
import org.jivesoftware.spark.ui.VCardPanel;
import org.jivesoftware.spark.ui.rooms.ChatRoomImpl;
import org.jivesoftware.spark.util.GraphicUtils;
import org.jivesoftware.spark.util.ModelUtil;
import org.jivesoftware.spark.util.SwingWorker;
import org.jivesoftware.spark.util.TaskEngine;
import org.jivesoftware.spark.util.UIComponentRegistry;
import org.jivesoftware.sparkimpl.settings.local.LocalPreferences;
import org.jivesoftware.sparkimpl.settings.local.SettingsManager;

import com.alee.laf.button.WebButton;
import com.kingray.spark.plugin.service.MessageService;
import com.kingray.spark.plugin.vo.MessageVO;
import com.xiongyingqi.util.ComparatorHelper;
import com.xiongyingqi.util.DomainHelper;

/**
 * 历史消息类，用于实现查看消息历史的功能
 * 
 * @author XiongYingqi
 * @version 2013-6-25 下午3:32:59
 */
public class MessageHistory implements ChatRoomListener {
	private final String timeFormat = "HH:mm:ss";
	private final String dateFormat = ((SimpleDateFormat) SimpleDateFormat
			.getDateInstance(SimpleDateFormat.FULL)).toPattern();
	private final SimpleDateFormat notificationDateFormatter;
	private final SimpleDateFormat messageDateFormatter;
	private HashMap<ChatRoom, Message> lastMessage = new HashMap<ChatRoom, Message>();
	private JDialog Frame;

	private static MessageHistory singleton;

	private MessageService messageService;

	public static MessageHistory getInstance() {
		if (singleton == null) {
			singleton = new MessageHistory();
		}
		return singleton;
	}

	private MessageHistory() {
		SparkManager.getChatManager().addChatRoomListener(this);

		messageService = MessageService.getInstance();

		notificationDateFormatter = new SimpleDateFormat(dateFormat);
		messageDateFormatter = new SimpleDateFormat(timeFormat);

		final ContactList contactList = SparkManager.getWorkspace()
				.getContactList();

		final Action viewHistoryAction = new AbstractAction() {
			private static final long serialVersionUID = -6498776252446416099L;

			public void actionPerformed(ActionEvent actionEvent) {
				ContactItem item = contactList.getSelectedUsers().iterator()
						.next();
				final String jid = item.getJID();

				showHistory(jid);
			}
		};

		viewHistoryAction.putValue(Action.NAME,
				Res.getString("menuitem.view.contact.history"));

		viewHistoryAction.putValue(Action.SMALL_ICON,
				SparkRes.getImageIcon(SparkRes.HISTORY_16x16));

		final Action showStatusMessageAction = new AbstractAction() {
			private static final long serialVersionUID = -5000370836304286019L;

			public void actionPerformed(ActionEvent actionEvent) {
				ContactItem item = contactList.getSelectedUsers().iterator()
						.next();
				showStatusMessage(item);
			}
		};

		showStatusMessageAction.putValue(Action.NAME,
				Res.getString("menuitem.show.contact.statusmessage"));

		contactList.addContextMenuListener(new ContextMenuListener() {
			public void poppingUp(Object object, JPopupMenu popup) {
				if (object instanceof ContactItem) {
					popup.add(viewHistoryAction);
					popup.add(showStatusMessageAction);
				}
			}

			public void poppingDown(JPopupMenu popup) {

			}

			public boolean handleDefaultAction(MouseEvent e) {
				return false;
			}
		});

		SparkManager.getMainWindow().addMainWindowListener(
				new MainWindowListener() {
					public void shutdown() {
					}

					public void mainWindowActivated() {

					}

					public void mainWindowDeactivated() {

					}
				});

		SparkManager.getConnection().addConnectionListener(
				new ConnectionListener() {
					public void connectionClosed() {
					}

					public void connectionClosedOnError(Exception e) {
						// persistConversations();
					}

					public void reconnectingIn(int i) {
					}

					public void reconnectionSuccessful() {
					}

					public void reconnectionFailed(Exception exception) {
					}
				});
	}

	/**
	 * <br>
	 * 2013-6-25 下午3:34:23
	 * 
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#chatRoomOpened(org.jivesoftware.spark.ui.ChatRoom)
	 */
	@Override
	public void chatRoomOpened(ChatRoom room) {
		LocalPreferences pref = SettingsManager.getLocalPreferences();
		if (!pref.isChatHistoryEnabled()) {
			return;
		}

		final String jid = room.getRoomname();

		if (room instanceof ChatRoomImpl) {
			new ChatRoomDecorator(room);
		}
		loadHistory(room);
	}

	/**
	 * <br>
	 * 2013-6-25 下午3:34:23
	 * 
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#chatRoomLeft(org.jivesoftware.spark.ui.ChatRoom)
	 */
	@Override
	public void chatRoomLeft(ChatRoom room) {

	}

	/**
	 * <br>
	 * 2013-6-25 下午3:34:23
	 * 
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#chatRoomClosed(org.jivesoftware.spark.ui.ChatRoom)
	 */
	@Override
	public void chatRoomClosed(ChatRoom room) {

	}

	/**
	 * <br>
	 * 2013-6-25 下午3:34:23
	 * 
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#chatRoomActivated(org.jivesoftware.spark.ui.ChatRoom)
	 */
	@Override
	public void chatRoomActivated(ChatRoom room) {

	}

	/**
	 * <br>
	 * 2013-6-25 下午3:34:23
	 * 
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#userHasJoined(org.jivesoftware.spark.ui.ChatRoom,
	 *      java.lang.String)
	 */
	@Override
	public void userHasJoined(ChatRoom room, String userid) {

	}

	/**
	 * <br>
	 * 2013-6-25 下午3:34:23
	 * 
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#userHasLeft(org.jivesoftware.spark.ui.ChatRoom,
	 *      java.lang.String)
	 */
	@Override
	public void userHasLeft(ChatRoom room, String userid) {

	}

	private void showHistory(final String jid) {

		SwingWorker transcriptLoader = new SwingWorker() {
			public Object construct() {
				String bareJID = StringUtils.parseBareAddress(jid);
				return messageService.queryMessageHistoriesByUserName(bareJID);
			}

			public void finished() {
				final JPanel mainPanel = new BackgroundPanel();

				mainPanel.setLayout(new BorderLayout());
				// add search text input
				final JPanel topPanel = new BackgroundPanel();
				topPanel.setLayout(new GridBagLayout());

				final VCardPanel vacardPanel = new VCardPanel(jid);

				final JPanel searchPanel = new JPanel();
				searchPanel.setLayout(new BorderLayout());

				final JTextField searchField = new JTextField(25);
				searchField
						.setText(Res.getString("message.search.for.history"));
				searchField.setToolTipText(Res
						.getString("message.search.for.history"));
				searchField.setForeground((Color) UIManager
						.get("TextField.lightforeground"));
				JButton searchButton = new WebButton("搜索");

				searchPanel.add(searchField, BorderLayout.CENTER);
				searchPanel.add(searchButton, BorderLayout.EAST);

				topPanel.add(vacardPanel, new GridBagConstraints(0, 0, 1, 1,
						1.0, 1.0, GridBagConstraints.NORTHWEST,
						GridBagConstraints.NONE, new Insets(1, 5, 1, 1), 0, 0));

				topPanel.add(searchPanel, new GridBagConstraints(1, 0,
						GridBagConstraints.REMAINDER, 1, 1.0, 1.0,
						GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
						new Insets(1, 1, 6, 1), 0, 0));

				// topPanel.add(searchField, new GridBagConstraints(1, 0,
				// GridBagConstraints.REMAINDER, 1, 1.0, 1.0,
				// GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				// new Insets(1, 1, 6, 1), 0, 0));
				// topPanel.add(searchButton, new GridBagConstraints(1, 0,
				// GridBagConstraints.REMAINDER, 1, 1.0, 1.0,
				// GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				// new Insets(1, 2, 6, 1), 0, 0));

				mainPanel.add(topPanel, BorderLayout.NORTH);

				final JEditorPane window = new JEditorPane();
				window.setEditorKit(new HTMLEditorKit());
				window.setBackground(Color.white);
				final JScrollPane pane = new JScrollPane(window);
				pane.getVerticalScrollBar().setBlockIncrement(200);
				pane.getVerticalScrollBar().setUnitIncrement(20);

				mainPanel.add(pane, BorderLayout.CENTER);

				final JFrame frame = new JFrame(Res.getString(
						"title.history.for", jid));
				frame.setIconImage(SparkRes
						.getImageIcon(SparkRes.HISTORY_16x16).getImage());
				frame.getContentPane().setLayout(new BorderLayout());

				frame.getContentPane().add(mainPanel, BorderLayout.CENTER);
				frame.pack();
				frame.setSize(600, 400);
				window.setCaretPosition(0);
				window.requestFocus();
				GraphicUtils.centerWindowOnScreen(frame);
				frame.setVisible(true);

				window.setEditable(false);

				final StringBuilder builder = new StringBuilder();
				builder.append("<html><body><table cellpadding=0 cellspacing=0>");

				final TimerTask transcriptTask = new TimerTask() {
					public void run() {
						// reduce the size of our transcript to the last
						// 5000Messages
						// This will prevent JavaOutOfHeap Errors
						Collection<MessageVO> messageVOs = messageService
								.searcheMessageHistoriesByMessageBody(
										jid,
										Res.getString(
												"message.search.for.history")
												.equals(searchField.getText()) ? null
												: searchField.getText());

						ComparatorHelper<MessageVO> helper = new ComparatorHelper<MessageVO>();
						try {
							messageVOs = helper
									.sortByDateTime(
											messageVOs,
											MessageVO.class
													.getDeclaredField("messageDateTime"),
											false);// 反向排序
						} catch (SecurityException e) {
							Log.error(e);
						} catch (NoSuchFieldException e) {
							Log.error(e);
						}
						// final String personalNickname = SparkManager
						// .getUserManager().getNickname();
						Date lastPost = null;
						String lastPerson = null;
						boolean initialized = false;
						for (MessageVO message : messageVOs) {
							String color = "blue";

							String from = message.getMessageFrom()
									.getUserName();
							String nickname = message.getMessageFrom()
									.getRealName();
							String body = org.jivesoftware.spark.util.StringUtils
									.escapeHTMLTags(message.getMessageBody());

							if (!StringUtils.parseBareAddress(from).equals(
									DomainHelper.removeDomain(SparkManager
											.getSessionManager()
											.getBareAddress()))) {
								color = "red";
							}

							long lastPostTime = lastPost != null ? lastPost
									.getTime() : 0;

							int diff = 0;
							if (DateUtils.getDaysDiff(lastPostTime, message
									.getMessageDateTime().getTime()) != 0) {
								diff = DateUtils.getDaysDiff(lastPostTime,
										message.getMessageDateTime().getTime());
							} else {
								diff = DateUtils.getDayOfWeek(lastPostTime)
										- DateUtils
												.getDayOfWeek(message
														.getMessageDateTime()
														.getTime());
							}

							if (diff != 0) {
								if (initialized) {
									builder.append("<tr><td><br></td></tr>");
								}
								builder.append(
										"<tr><td colspan=2><font size=4 color=gray><b><u>")
										.append(notificationDateFormatter
												.format(message
														.getMessageDateTime()))
										.append("</u></b></font></td></tr>");
								lastPerson = null;
								initialized = true;
							}

							String value = "["
									+ messageDateFormatter.format(message
											.getMessageDateTime())
									+ "]&nbsp;&nbsp;  ";

							boolean newInsertions = lastPerson == null
									|| !lastPerson.equals(nickname);
							if (newInsertions) {
								builder.append("<tr valign=top><td colspan=2 nowrap>");
								builder.append("<font size=4 color='")
										.append(color).append("'><b>");
								builder.append(nickname);
								builder.append("</b></font>");
								builder.append("</td></tr>");
							}

							builder.append("<tr valign=top><td align=left nowrap>");
							builder.append(value);
							builder.append("</td><td align=left>");
							// if(body.length() > 200){ // 如果长度大于200，则截取字符串
							// builder.append(body.substring(0, 200)); // TODO
							// }
							builder.append(body);

							builder.append("</td></tr>");

							lastPost = message.getMessageDateTime();
							lastPerson = nickname;
						}
						builder.append("</table></body></html>");

						// Handle no history
						if (messageVOs.size() == 0) {
							builder.append("<b>")
									.append(Res
											.getString("message.no.history.found"))
									.append("</b>");
						}

						window.setText(builder.toString());
						builder.replace(0, builder.length(), "");

					}
				};

				searchButton.addMouseListener(new MouseAdapter() {
					/**
					 * <br>
					 * 2013-6-25 下午9:18:17
					 * 
					 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
					 */
					@Override
					public void mouseClicked(MouseEvent e) {
						TaskEngine.getInstance().schedule(transcriptTask, 10);
						searchField.requestFocus();
					}
				});

				searchField.addKeyListener(new KeyListener() {
					@Override
					public void keyTyped(KeyEvent e) {
					}

					@Override
					public void keyReleased(KeyEvent e) {
						if (e.getKeyChar() == KeyEvent.VK_ENTER) {
							TaskEngine.getInstance().schedule(transcriptTask,
									10);
							searchField.requestFocus();
						}
					}

					@Override
					public void keyPressed(KeyEvent e) {

					}
				});
				searchField.addFocusListener(new FocusListener() {
					public void focusGained(FocusEvent e) {
						if (Res.getString("message.search.for.history").equals(
								searchField.getText().trim())) {
							searchField.setText("");
							searchField.setForeground((Color) UIManager
									.get("TextField.foreground"));
						}
					}

					public void focusLost(FocusEvent e) {
						if ("".equals(searchField.getText().trim())) {
							searchField.setForeground((Color) UIManager
									.get("TextField.lightforeground"));
							searchField.setText(Res
									.getString("message.search.for.history"));
						}
					}
				});

				TaskEngine.getInstance().schedule(transcriptTask, 10);

				frame.addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						window.setText("");
					}

					@Override
					public void windowClosed(WindowEvent e) {
						frame.removeWindowListener(this);
						frame.dispose();
						transcriptTask.cancel();
						topPanel.remove(vacardPanel);
					}
				});
			}
		};
		transcriptLoader.start();
	}

	protected void loadHistory(ChatRoom room) {// canceled by XiongYingqi
		// Add VCard Panel
		VCardPanel vcardPanel = new VCardPanel(room.getRoomname());
		room.getToolBar().add(
				vcardPanel,
				new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 2, 0, 2),
						0, 0));

		// final ChatTranscript chatTranscript = ChatTranscripts
		// .getCurrentChatTranscript(getParticipantJID());

		Collection<MessageVO> messageVOs = messageService
				.queryMessageHistoriesByUserName(room.getRoomname(), 100, true);
		ComparatorHelper<MessageVO> comparatorHelper = new ComparatorHelper<MessageVO>();

		try {
			messageVOs = comparatorHelper.sortByDateTime(messageVOs,
					MessageVO.class.getDeclaredField("messageDateTime"), true);
		} catch (SecurityException e) {
			Log.error(e);
		} catch (NoSuchFieldException e) {
			Log.error(e);
		}

		room.getTranscriptWindow().insertCustomText("更多历史消息请点击\"查看会话历史记录\"",
				true, true, Color.PINK); // 插入提示消息

		final String personalNickname = SparkManager.getUserManager()
				.getNickname();

		for (MessageVO message : messageVOs) {
			String nickname = message.getMessageFrom().getRealName();
			String messageBody = message.getMessageBody();
			// if (nickname.equals(message.getFrom())) {
			// String otherJID = StringUtils.parseBareAddress(message
			// .getFrom());
			// String myJID = SparkManager.getSessionManager()
			// .getBareAddress();
			//
			// if (otherJID.equals(myJID)) {
			// nickname = personalNickname;
			// } else {
			// try {
			// nickname = message.getFrom().substring(
			// message.getFrom().indexOf("/") + 1);
			// } catch (Exception e) {
			// nickname = StringUtils.parseName(nickname);
			// }
			// }
			// }

			if (ModelUtil.hasLength(messageBody)
					&& messageBody.startsWith("/me ")) {
				messageBody = messageBody.replaceFirst("/me", nickname);
			}

			final Date messageDate = message.getMessageDateTime();
			room.getTranscriptWindow().insertHistoryMessage(nickname,
					messageBody, messageDate);
		}
		if (0 < messageVOs.size()) { // Check if we have
			room.getTranscriptWindow().insertHorizontalLine();
		}
		// chatTranscript.release();
	}

	private void showStatusMessage(ContactItem item) {
		Frame = new JDialog();
		Frame.setTitle(item.getDisplayName() + " - Status");
		JPanel pane = new JPanel();
		JTextArea textArea = new JTextArea(5, 30);
		JButton btn_close = new JButton(Res.getString("button.close"));

		btn_close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Frame.setVisible(false);
			}
		});

		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);

		pane.add(new JScrollPane(textArea));
		Frame.setLayout(new BorderLayout());
		Frame.add(pane, BorderLayout.CENTER);
		Frame.add(btn_close, BorderLayout.SOUTH);

		textArea.setEditable(false);
		textArea.setText(item.getStatus());

		Frame.setLocationRelativeTo(SparkManager.getMainWindow());
		Frame.setBounds(Frame.getX() - 175, Frame.getY() - 75, 350, 150);
		Frame.setSize(350, 150);
		Frame.setResizable(false);
		Frame.setVisible(true);
	}

	private class ChatRoomDecorator implements ActionListener,
			ChatRoomClosingListener {

		private ChatRoom chatRoom;
		private ChatRoomButton chatHistoryButton;
		private final LocalPreferences localPreferences;

		public ChatRoomDecorator(ChatRoom chatRoom) {
			this.chatRoom = chatRoom;
			chatRoom.addClosingListener(this);

			// Add History Button
			localPreferences = SettingsManager.getLocalPreferences();
			if (!localPreferences.isChatHistoryEnabled()) {
				return;
			}

			chatHistoryButton = UIComponentRegistry.getButtonFactory()
					.createChatTranscriptButton();
			chatRoom.addChatRoomButton(chatHistoryButton);
			chatHistoryButton.setToolTipText(Res
					.getString("tooltip.view.history"));
			chatHistoryButton.addActionListener(this);
		}

		public void closing() {
			if (localPreferences.isChatHistoryEnabled()) {
				chatHistoryButton.removeActionListener(this);
			}
			chatRoom.removeClosingListener(this);
			chatRoom = null;
			chatHistoryButton = null;
		}

		public void actionPerformed(ActionEvent e) {
			ChatRoomImpl roomImpl = (ChatRoomImpl) chatRoom;
			showHistory(roomImpl.getParticipantJID());
		}
	}

}
