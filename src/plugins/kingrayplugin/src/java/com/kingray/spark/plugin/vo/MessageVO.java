package com.kingray.spark.plugin.vo;


import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;

import com.kingray.spark.packet.IQElementMapping;
import com.xiongyingqi.utils.DateHelper;
import com.xiongyingqi.utils.EntityHelper;
import com.xiongyingqi.vo.VO;

/**
 * 
 * @author KRXiongYingqi
 */
@IQElementMapping("message")
public class MessageVO extends EntityHelper implements VO{
	@IQElementMapping("messageId")
	private long messageId;
	
	@IQElementMapping("sendMessageId")
	private String sendMessageId;
	
	private UserVO messageFrom;
	
	@IQElementMapping("messageFrom")
	private String messageFromStr;
	
	private UserVO messageTo;
	
	@IQElementMapping("messageTo")
	private String messageToStr;
	
	@IQElementMapping("messageSubject")
	private String messageSubject;
	
	@IQElementMapping("messageBody")
	private String messageBody;
	
//	@IQElementMapping("messageDateTime") // 取消
	private Date messageDateTime;
	
	@IQElementMapping("messageDateTimeStr")
	private String messageDateTimeStr;
	
	@IQElementMapping("messageType")
	private String messageType;
	
//	private String realName;
	
	
//	public MessageVO(long messageId, String sendMessageId, String messageFrom, String messageTo, String messageBody){
//		this.messageId = messageId;
//		this.sendMessageId = sendMessageId;
//		this.messageFrom = messageFrom;
//		this.messageTo = messageTo;
//		this.messageBody = messageBody;
//	}
	
	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return messageType;
	}
	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	/**
	 * @return the messageSubject
	 */
	public String getMessageSubject() {
		return messageSubject;
	}
	/**
	 * @param messageSubject the messageSubject to set
	 */
	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}
	/**
	 * @return the messageId
	 */
	public long getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	/**
	 * @return the sendMessageId
	 */
	public String getSendMessageId() {
		return sendMessageId;
	}
	/**
	 * @param sendMessageId the sendMessageId to set
	 */
	public void setSendMessageId(String sendMessageId) {
		this.sendMessageId = sendMessageId;
	}
//	/**
//	 * @return the messageFrom
//	 */
//	public String getMessageFrom() {
//		return messageFrom;
//	}
//	/**
//	 * @param messageFrom the messageFrom to set
//	 */
//	public void setMessageFrom(String messageFrom) {
//		if(messageFrom == null) messageFrom = "";
//		this.messageFrom = messageFrom;
//	}
//	/**
//	 * @return the messageTo
//	 */
//	public String getMessageTo() {
//		return messageTo;
//	}
//	/**
//	 * @param messageTo the messageTo to set
//	 */
//	public void setMessageTo(String messageTo) {
//		if(messageTo == null) messageTo = "";
//		this.messageTo = messageTo;
//	}
	/**
	 * @return the messageBody
	 */
	public String getMessageBody() {
		return messageBody;
	}
	/**
	 * @param messageBody the messageBody to set
	 */
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	/**
	 * @return the messageDateTime
	 */
	public Date getMessageDateTime() {
		return messageDateTime;
	}
	/**
	 * @param messageDateTime the messageDateTime to set
	 */
	public void setMessageDateTime(Date messageDateTime) {
		this.messageDateTimeStr = DateHelper.FORMATTER_LONG.format(messageDateTime);
		this.messageDateTime = messageDateTime;
	}
	
//	/**
//	 * @return the userName
//	 */
//	public String getRealName() {
//		return realName;
//	}
//	/**
//	 * @param userName the userName to set
//	 */
//	public void setRealName(String realName) {
//		this.realName = realName;
//	}
	/**
	 * @return the messageDateTimeStr
	 */
	public String getMessageDateTimeStr() {
		return messageDateTimeStr;
	}
	/**
	 * @param messageDateTimeStr the messageDateTimeStr to set
	 */
	public void setMessageDateTimeStr(String messageDateTimeStr) {
		this.messageDateTime = DateHelper.strToDateLong(messageDateTimeStr);
		this.messageDateTimeStr = messageDateTimeStr;
	}
	
	/**
	 * @return the messageFrom
	 */
	public UserVO getMessageFrom() {
		return messageFrom;
	}

	/**
	 * @param messageFrom the messageFrom to set
	 */
	public void setMessageFrom(UserVO messageFrom) {
		this.messageFrom = messageFrom;
	}

	/**
	 * @return the messageTo
	 */
	public UserVO getMessageTo() {
		return messageTo;
	}

	/**
	 * @param messageTo the messageTo to set
	 */
	public void setMessageTo(UserVO messageTo) {
		this.messageTo = messageTo;
	}
	
	/**
	 * @param messageFromStr the messageFromStr to set
	 */
	public void setMessageFromStr(String messageFromStr) {
		this.messageFromStr = messageFromStr;
		if(messageFromStr != null){
			setMessageFrom(new UserVO(messageFromStr));
		}
	}

	/**
	 * @param messageToStr the messageToStr to set
	 */
	public void setMessageToStr(String messageToStr) {
		this.messageToStr = messageToStr;
		if(messageToStr != null){
			setMessageTo(new UserVO(messageToStr));
		}
	}

	/**
	 * String
	 * @return the messageFromStr
	 */
	public String getMessageFromStr() {
		return messageFromStr;
	}

	/**
	 * String
	 * @return the messageToStr
	 */
	public String getMessageToStr() {
		return messageToStr;
	}
	
	/**
	 * 重写hashCode函数，以sendMessageId作为唯一识别对象
	 */
	@Override
	public int hashCode() {
		if(sendMessageId != null){
			return sendMessageId.hashCode();
		}
		return super.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if(obj != null && (obj instanceof MessageVO)){
			return this.hashCode() == obj.hashCode();
		}
		return super.equals(obj);
	}

	public static void main(String[] args) {
		Collection<MessageVO> messageVOs = new LinkedHashSet<MessageVO>();
		for (int i = 0; i < 100; i++) {
			MessageVO messageVO = new MessageVO();
			messageVO.setSendMessageId("" + i);
			messageVOs.add(messageVO);
			messageVOs.add(messageVO);
		}
		messageVOs.addAll(messageVOs);
		
		
		for (int i = 0; i < 100; i++) {
			MessageVO messageVO = new MessageVO();
			messageVO.setSendMessageId("" + i);
			messageVOs.remove(messageVO);
		}
		System.out.println(messageVOs.size());
	}

}
