package com.kingray.spark.plugin.packet;

import java.util.Collection;
import java.util.HashSet;

import org.dom4j.Element;
import org.jivesoftware.smack.packet.IQ;

import com.kingray.spark.packet.IQElementMapping;
import com.kingray.spark.plugin.vo.MessageVO;
import com.xiongyingqi.convert.XmlConvert;
import com.xiongyingqi.utils.StringHelper;

public class MessageHistoryResultIQ extends IQ {
	
	private int messageCount = -1;
	
	private String relationUserName;
	
	private Collection<MessageVO> messageVOs;
	
	/**
	 * xml转换器
	 */
	private XmlConvert convert;
	
	public MessageHistoryResultIQ(){
		convert = new XmlConvert(IQElementMapping.class);
	}
	/**
	 * int
	 * @return the messageCount
	 */
	public int getMessageCount() {
		return messageCount;
	}

	/**
	 * @param messageCount the messageCount to set
	 */
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	/**
	 * String
	 * @return the relationUserName
	 */
	public String getRelationUserName() {
		return relationUserName;
	}

	/**
	 * @param relationUserName the relationUserName to set
	 */
	public void setRelationUserName(String relationUserName) {
		this.relationUserName = relationUserName;
	}

	/**
	 * Collection<MessageVO>
	 * @return the messageVOs
	 */
	public Collection<MessageVO> getMessageVOs() {
		return messageVOs;
	}

	/**
	 * @param messageVOs the messageVOs to set
	 */
	public void setMessageVOs(Collection<MessageVO> messageVOs) {
		this.messageVOs = messageVOs;
	}
	/**
	 *  
	 * @param messageVO
	 */
	public void addMessageVO(MessageVO messageVO){
		if(this.messageVOs == null){
			this.messageVOs = new HashSet<MessageVO>();
		}
		this.messageVOs.add(messageVO);
	}

	@Override
	public String getChildElementXML() {
		Element queryElement = convert.convertVOs(messageVOs);
		if(messageCount >= 0){
			queryElement.addAttribute("messageCount", messageCount + "");
		}
		if(StringHelper.notNullAndNotEmpty(relationUserName)){
			queryElement.addAttribute("relationUserName", relationUserName);
		}
		return queryElement.asXML();
	}

}
