package com.kingray.spark.plugin.listener;

import java.util.Date;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.MessageEventManager;
import org.jivesoftware.smackx.MessageEventNotificationListener;
import org.jivesoftware.smackx.MessageEventRequestListener;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.spark.ui.ChatRoom;
import org.jivesoftware.spark.ui.GlobalMessageListener;
import org.jivesoftware.spark.ui.MessageFilter;
import org.jivesoftware.spark.util.log.Log;

import com.kingray.spark.plugin.handle.MessageHandler;
import com.kingray.spark.plugin.vo.MessageVO;
import com.kingray.spark.plugin.vo.UserVO;
import com.xiongyingqi.util.DomainHelper;
import com.xiongyingqi.util.EntityHelper;
import com.xiongyingqi.util.StackTraceHelper;
/**
 * 消息监听类，获取用户发送或接收的消息
 * @author XiongYingqi
 *
 */
public class KingrayMessageListener implements GlobalMessageListener{
	private MessageHandler messageHandler;
	public KingrayMessageListener() {
		messageHandler = MessageHandler.getInstance();
	}
//	@Override
//	public void filterOutgoing(ChatRoom room, Message message) {
//		doGetMessage(room, message);
//	}
//
//	@Override
//	public void filterIncoming(ChatRoom room, Message message) {
//		doGetMessage(room, message);
//	}
	private void doGetMessage(MessageVO messageVO){
		messageHandler.handleMessage(messageVO);
	}
	
	@Override
	public void messageReceived(ChatRoom room, Message message) {
		try {
			StackTraceHelper.printStackTrace();
			
			String messageFrom = DomainHelper.removeDomain(room.getRoomname()); // 获取发送人
			String messageTo = DomainHelper.removeDomain(room.getNickname()); // 获取接受人
//		String userName = DomainHelper.removeDomain(room.getRoomTitle()); // 获取接受人姓名
			
			String packetId = message.getPacketID(); // 获取消息ID
			String messageBody = message.getBody(); // 消息类型
			Message.Type messageType = room.getChatType(); // 获取消息类型
			Date messageDate = new Date(); // 发送时间
//		MessageVO messageVO = new MessageVO(0, packetId, messageFrom, messageTo, messageBody);
			MessageVO messageVO = new MessageVO();
			messageVO.setMessageFrom(new UserVO(messageFrom));
			messageVO.setMessageTo(new UserVO(messageTo));
			messageVO.setSendMessageId(packetId);
			messageVO.setMessageBody(messageBody);
			messageVO.setMessageDateTime(messageDate);
			messageVO.setMessageType(messageType + "");
//		messageVO.setRealName(userName);
			
			EntityHelper.print(messageVO);
			
			doGetMessage(messageVO);
		} catch (Exception e) {
			Log.error(e);
			e.printStackTrace();
		}
	}
	@Override
	public void messageSent(ChatRoom room, Message message) {
		String messageFrom = DomainHelper.removeDomain(room.getNickname()); // 获取发送人
		String messageTo = DomainHelper.removeDomain(room.getRoomname()); // 获取接受人
//		String userName = DomainHelper.removeDomain(room.getRoomTitle()); // 获取接受人姓名
		String packetId = message.getPacketID(); // 获取消息ID
		String messageBody = message.getBody(); // 消息类型
		Message.Type messageType = room.getChatType(); // 获取消息类型
		Date messageDate = new Date(); // 发送时间
		MessageVO messageVO = new MessageVO();
		messageVO.setMessageFrom(new UserVO(messageFrom));
		messageVO.setMessageTo(new UserVO(messageTo));
		messageVO.setSendMessageId(packetId);
		messageVO.setMessageBody(messageBody);
		messageVO.setMessageDateTime(messageDate);
		messageVO.setMessageType(messageType + "");
		doGetMessage(messageVO);
	}
}
