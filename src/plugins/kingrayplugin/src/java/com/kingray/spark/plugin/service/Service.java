package com.kingray.spark.plugin.service;

public interface Service {
	/**
	 * 要被服务的方法，当service实现本类时必须调用ServiceThread#addService()方法
	 * <br>2013-6-24 下午4:43:02
	 * @see com.kingray.spark.plugin.service.ServiceThread#addService() 
	 */
	public void startService() throws ServiceException;
}
