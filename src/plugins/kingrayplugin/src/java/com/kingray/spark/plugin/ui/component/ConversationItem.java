package com.kingray.spark.plugin.ui.component;

import java.awt.Color;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

import javax.swing.JPanel;

import com.alee.extended.panel.FlowPanel;
import com.alee.managers.tooltip.TooltipManager;
import com.alee.managers.tooltip.TooltipWay;
import com.kingray.spark.plugin.vo.UserVO;
import com.xiongyingqi.util.CalendarHelper;
import com.xiongyingqi.util.DateHelper;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;

import org.jivesoftware.resource.SparkRes;
import org.jivesoftware.smackx.packet.Time;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.spark.util.Base64;
import org.jivesoftware.spark.util.GraphicUtils;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * 单个会话条目
 * 
 * @author XiongYingqi
 * 
 */
public class ConversationItem extends JPanel implements MouseListener{
	// public static final int IMAGE_WIDTH = 45; // 图像宽
	// public static final int IMAGE_HEIGHT = 45; // 图像高
	// public static final int IMAGE_TOP = 5; // 与条目的顶端距离
	// public static final int IMAGE_LEFT = 5; // 与条目的左端距离
	// public static final int IMAGE_DOWN = 5; // 与条目底部的距离
	// public static final int IMAGE_RIGHT = 5; // 与消息内容的距离
	// public static final int MESSAGE_DATE_LABEL_WIDTH = 30; // 日期标签的宽度

	private static final Color COLOR_MOUSE_ENTER = new Color(255, 189, 63, 255);
	private static final Color COLOR_MOUSE_LEAVE = new Color(255, 255, 255, 255);
	private static final Color COLOR_ALPHA = new Color(255, 255, 255, 0);// 透明颜色

	private JLabel imageLabel; // 图像标签
	private JPanel rightPanel; // 右部面板
	private JLabel messageFromLabel; // 发送人标签
	private JLabel messageDateTimeLabel; // 会话时间标签
	private JLabel messageBodyLabel; // 会话内容标签
	private JPanel upPanel; // 右部面板的上半部面板
	private JPanel lowerPanel; // 右部面板的下半部面板
	private JPanel leftPanel; // 左部面板
	private JLabel blockLabel2; // 图像上部空标签
	private JLabel blockLabel; // 图像下部空标签

	private Date messageDateTime; // 会话日期
	private String messageFrom;
	private JPanel southPanel;

	public ConversationItem(int width) {
		// setLayout(null);
		// this.setBorder(new LineBorder(Color.gray));
		setBackground(COLOR_MOUSE_LEAVE);
		this.setSize(width, 55);
		setOpaque(true);
		setLayout(new BorderLayout(0, 0));

		leftPanel = new MyAlphaPanel();
		// leftPanel.setBackground(COLOR_MOUSE_LEAVE);
		add(leftPanel, BorderLayout.WEST);
		leftPanel.setLayout(new BorderLayout(0, 0));

		blockLabel2 = new JLabel("   ");
		blockLabel2.setFont(new Font("微软雅黑", Font.PLAIN, 1));
		blockLabel2.setBackground(COLOR_ALPHA);
		leftPanel.add(blockLabel2, BorderLayout.NORTH);
		// System.out.println("this.getWidth() =================== " +
		// this.getWidth());
		imageLabel = new JLabel();
		leftPanel.add(imageLabel);

		blockLabel = new JLabel("   ");
		blockLabel.setFont(new Font("微软雅黑", Font.PLAIN, 1));
		blockLabel.setBackground(COLOR_ALPHA);
		leftPanel.add(blockLabel, BorderLayout.SOUTH);

		rightPanel = new MyAlphaPanel();
		// rightPanel.setBackground(COLOR_MOUSE_LEAVE);
		rightPanel.setLayout(new GridLayout(0, 1, 0, 0));

		upPanel = new MyAlphaPanel();
		// upPanel.setBackground(COLOR_MOUSE_LEAVE);
		rightPanel.add(upPanel);
		upPanel.setLayout(new BorderLayout(0, 0));
		// rightPanel.setLayout(null);
		// rightPanel.setBounds(IMAGE_LEFT + IMAGE_WIDTH, 0, this.getWidth() -
		// (IMAGE_LEFT + IMAGE_WIDTH), this.getHeight());

		messageFromLabel = new JLabel();
		messageFromLabel.setFont(new Font("微软雅黑", Font.PLAIN, 12));
		messageFromLabel.setHorizontalAlignment(SwingConstants.LEFT);
		upPanel.add(messageFromLabel, BorderLayout.CENTER);

		messageDateTimeLabel = new JLabel();
		messageDateTimeLabel.setForeground(Color.gray);
		messageDateTimeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		upPanel.add(messageDateTimeLabel, BorderLayout.EAST);
		add(rightPanel, BorderLayout.CENTER);

		lowerPanel = new MyAlphaPanel();
		// lowerPanel.setBackground(COLOR_MOUSE_LEAVE);
		rightPanel.add(lowerPanel);
		lowerPanel.setLayout(new GridLayout(0, 1, 0, 0));

		messageBodyLabel = new JLabel();
		messageBodyLabel.setFont(new Font("微软雅黑", Font.PLAIN, 12));
		messageBodyLabel.setForeground(Color.gray);
		lowerPanel.add(messageBodyLabel);

		southPanel = new MyAlphaPanel();
		// southPanel.setBackground(COLOR_MOUSE_LEAVE);
		southPanel.setLayout(new BorderLayout(0, 0));
		southPanel.add(new JSeparator());
		add(southPanel, BorderLayout.SOUTH);

		this.addMouseListener(this);

	}
	

	/**
	 * <br>
	 * 2013-6-26 上午11:44:02
	 * 
	 * @see java.awt.Container#add(java.awt.Component)
	 */
	@Override
	public Component add(Component comp) {
		comp.addMouseListener(ConversationItem.this);
		return super.add(comp);
	}

	@Override
	public void validate() {
		// rightPanel.setBounds(IMAGE_LEFT + IMAGE_WIDTH, 0, this.getWidth() -
		// (IMAGE_LEFT + IMAGE_WIDTH), this.getHeight());
		// messageFromLabel.setBounds(0, 0, rightPanel.getWidth() -
		// MESSAGE_DATE_LABEL_WIDTH, rightPanel.getHeight() / 2);
		// messageDateTimeLabel.setBounds(messageFromLabel.getWidth(), 0,
		// MESSAGE_DATE_LABEL_WIDTH, rightPanel.getHeight() / 2);
		// messageBodyLabel.setBounds(0, rightPanel.getHeight() / 2,
		// rightPanel.getWidth(), rightPanel.getHeight() / 2);
		super.validate();
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
		String host = SparkManager.getConnection().getHost();
		// String serviceName = SparkManager.getConnection().getServiceName();
		VCard vCard = SparkManager.getVCardManager().getVCard(
				messageFrom + "@" + host);
		byte[] imageData = null;
		if (vCard != null) {
			imageData = vCard.getAvatar();
		}
		ImageIcon imageIcon;
		if (imageData != null && imageData.length > 0) {
			imageIcon = new ImageIcon(imageData);
		} else {
			imageIcon = SparkRes
					.getImageIcon(SparkRes.DEFAULT_AVATAR_64x64_IMAGE);
		}
		ImageIcon conversationIcon = GraphicUtils.scaleImageIcon(imageIcon, 45,
				45);
		this.imageLabel.setIcon(conversationIcon);
	}

	public void setMessageConversationName(String conversationName) {
		this.messageFromLabel.setText("  " + conversationName);// 设置为昵称
	}

	public void setMessageBody(String messageBody) {
		this.messageBodyLabel.setText("  " + messageBody);
		this.messageBodyLabel.setToolTipText(messageBody);
//		TooltipManager.setTooltip(this.messageBodyLabel, messageBody, TooltipWay.down, 0);
		this.validate();
	}

	public String getConversationName() {
		return this.messageFrom;
	}

	public void setMessageDateTime(Date messageDateTime) {
		if (messageDateTime == null) {
			messageDateTime = new Date();
		}
		this.messageDateTime = messageDateTime;
		refreshDateLabel();
	}

	/**
	 * 刷新日期显示
	 */
	public void refreshDateLabel() {
		if (this.messageDateTime != null) {
			messageDateTimeLabel.setText(CalendarHelper
					.caculatorDateToString(messageDateTime));
			validate();
		}
	}

	/**
	 * @return the messageDateTime
	 */
	public Date getMessageDateTime() {
		return messageDateTime;
	}

	class MyAlphaPanel extends JPanel {
		MyAlphaPanel() {
			super(true);
			this.setBackground(COLOR_ALPHA);
		}
		/**
		 * <br>
		 * 2013-6-26 上午11:44:02
		 * 
		 * @see java.awt.Container#add(java.awt.Component)
		 */
		@Override
		public Component add(Component comp) {
			comp.addMouseListener(ConversationItem.this);
			return super.add(comp);
		}
	}

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(DateHelper.strToDateLong("2013-06-08 10:10:10"));
		int compare = calendar.compareTo(calendar2);
		int compare2 = calendar2.compareTo(calendar);
		System.out.println(CalendarHelper.compareTwoDate(
				DateHelper.strToDateLong("2013-06-08 10:10:10"), new Date()));
		// System.out.println(compare2);
	}


	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
		ConversationItem.this.setBackground(COLOR_MOUSE_LEAVE);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		ConversationItem.this.setBackground(COLOR_MOUSE_ENTER);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			String jid = SparkManager.getUserManager().getValidJID(
					messageFrom) + "@" + SparkManager.getSessionManager().getServerAddress();
			final String contactUsername = SparkManager
					.getUserManager().getUserNicknameFromJID(jid);
			SparkManager.getChatManager().activateChat(jid,
					contactUsername);
			// SparkManager.getChatManager().getChatRoom(jid).setVisible(true);
		}

	}

}
