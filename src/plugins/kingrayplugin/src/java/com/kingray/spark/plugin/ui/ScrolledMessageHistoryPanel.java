package com.kingray.spark.plugin.ui;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import com.alee.laf.scroll.WebScrollPane;
/**
 * 可滚动的面板，子面板必须设置内容大小。使用setPreferredSize()、setPreferredWidth()或者setPreferredHeight()
 * @author XiongYingqi
 *
 */
public class ScrolledMessageHistoryPanel extends WebScrollPane{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7789699771560172160L;

	public ScrolledMessageHistoryPanel() {
		super(MessageHistoryPanel.getInstance());
//		super(MessageHistoryPanel.getInstance(), WebScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, WebScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
//		MessageHistoryPanel.getInstance().setPreferredSize(new Dimension(this.getWidth(), MessageHistoryPanel.getInstance().getHeight()));
		setHorizontalScrollBarPolicy(WebScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		this.addMouseWheelListener(new MouseWheelListener() {
			
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				MessageHistoryPanel.getInstance().validate();
			}
		});
//		setBorder(null);
//		setMargin(0);
//		setShadeWidth(0);
//		setRound(0);
//		setDrawBorder(false);
		//		MessageHistoryPanel messageHistoryPanel = MessageHistoryPanel.getInstance();
//		this.setHorizontalScrollBarPolicy();
//		messageHistoryPanel.setSize(this.getSize().width, this.getSize().height);
	}

}
