package com.kingray.spark.plugin.handle;

import org.jivesoftware.spark.util.log.Log;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.packet.Message;

import com.kingray.spark.plugin.KingrayPlugin;
import com.kingray.spark.plugin.dao.MessageHistoryDAO;
import com.kingray.spark.plugin.dao.impl.MessageHistoryDAOImpl;
import com.kingray.spark.plugin.ui.MessageHistoryPanel;
import com.kingray.spark.plugin.vo.MessageVO;
import com.xiongyingqi.cache.Cache;
import com.xiongyingqi.cache.HashMapCache;
import com.xiongyingqi.cache.CacheException;
import com.xiongyingqi.cache.CacheFactory;
import com.xiongyingqi.cache.CacheListener;
import com.xiongyingqi.util.CalendarHelper;
import com.xiongyingqi.util.MessageVOHelper;

/**
 * 消息处理中心，用于分配用户发送的消息，可能存到缓存或者存储到数据库，另一方面处理UI的会话记录刷新
 * 
 * @author XiongYingqi
 * 
 */
public class MessageHandler implements CacheListener {
	public static final int MAX_CACHE_SIZE = 20; // 最大的缓存量，如果超过该数值则马上提交数据修改
	public static final int MAX_GROUP_CACHE_SIZE = 200; // 分组会话最大的缓存量，如果超过该数值则马上提交数据修改
	private final long DISPATCHER_MESSAGE_CACHE_PERIOD = 30 * CalendarHelper.SECOND; // 执行提交消息缓存的时间间隔
	private static final Object LOCK = new Object(); // 锁

	private MessageHistoryDAO messageHistoryDao; // 存储数据库服务

	private HashMapCache<String, Set<MessageVO>> messageVOCache;
	private HashMapCache<String, Set<MessageVO>> groupMessageVOCache;

	// private HashSetCache<MessageVO> messageVOCache; // 存储消息缓存
	// private HashSetCache<MessageVO> groupMessageVOCache; // 存储组聊天的缓存

	private static MessageHandler singleton;

	@SuppressWarnings("unchecked")
	private MessageHandler() {
		messageHistoryDao = MessageHistoryDAOImpl.getInstance(KingrayPlugin
				.getMessageHistoryPath());
		if (messageVOCache == null) {
			// messageVOCache = new HashSet<MessageVO>();
			messageVOCache = (HashMapCache<String, Set<MessageVO>>) CacheFactory
					.getCache("消息缓存", HashMapCache.class);
			messageVOCache.addCacheListener(this);
		}
		if (groupMessageVOCache == null) {
			groupMessageVOCache = (HashMapCache<String, Set<MessageVO>>) CacheFactory
					.getCache("分组消息缓存", HashMapCache.class);
			groupMessageVOCache.addCacheListener(this);
			// new HashSet<MessageVO>()
		}

		startTimer();
	}

	public static MessageHandler getInstance() {
		synchronized (LOCK) {
			if (singleton == null) {
				singleton = new MessageHandler();
			}
		}
		return singleton;
	}

	/**
	 * 处理消息，该逻辑由本类来判断是否要存储到数据库还是缓存
	 * 
	 * @param messageVO
	 */
	public void handleMessage(MessageVO messageVO) {
		String relationUserName = MessageVOHelper
				.getRelationUserName(messageVO);
		if (messageVO.getMessageType() != null
				&& Message.Type.groupchat.equals(messageVO.getMessageType())) {
			groupMessageVOCache.get(relationUserName).add(messageVO);
			if (groupMessageVOCache.get(relationUserName) != null) {
				groupMessageVOCache.get(relationUserName).add(messageVO);
			} else {
				Set<MessageVO> messageVOs = new LinkedHashSet();
				messageVOs.add(messageVO);
				groupMessageVOCache.put(relationUserName, messageVOs);
			}

		} else {
			if (messageVOCache.get(relationUserName) != null) {
				messageVOCache.get(relationUserName).add(messageVO);
			} else {
				Set<MessageVO> messageVOs = new LinkedHashSet();
				messageVOs.add(messageVO);
				messageVOCache.put(relationUserName, messageVOs);
			}
		}
		if (messageVOCache.size() >= MAX_CACHE_SIZE) {
			dispatcherMessageVOsInThread();
		}
		if (groupMessageVOCache.size() >= MAX_GROUP_CACHE_SIZE) {
			dispatcherGroupMessageVOsInThread();
		}
		try {
			MessageHistoryPanel.getInstance().addConversation(messageVO); // 通知会话改变
		} catch (Exception e) {
			Log.error(e);
		}
		// messageHistoryDao.insertMessageHistory(messageVO);
	}

	/**
	 * 在线程内提交消息缓存
	 */
	public void dispatcherMessageVOsInThread() {
		Thread thread = new Thread(new DispatcherMessageVOThread());
		thread.start();
	}

	/**
	 * 在线程内提交组消息缓存
	 */
	public void dispatcherGroupMessageVOsInThread() {
		Thread thread = new Thread(new DispatcherGroupMessageVOThread());
		thread.start();
	}

	/**
	 * 开始计时任务
	 */
	public void startTimer() {
		Timer timer = new Timer("MessageVOCacheTimer", true);
		DispatcherMessageHistoryCacheTimerTask cacheTimerTask = new DispatcherMessageHistoryCacheTimerTask();
		timer.schedule(cacheTimerTask, DISPATCHER_MESSAGE_CACHE_PERIOD,
				DISPATCHER_MESSAGE_CACHE_PERIOD);
	}

	class DispatcherMessageVOThread implements Runnable {
		@Override
		public void run() {
			dispatcherMessageVos();
		}

	}

	/**
	 * 提交消息缓存数据到数据库
	 */
	public void dispatcherMessageVos() {
		if (messageVOCache == null || messageVOCache.size() == 0) {
			return;
		}
		synchronized (LOCK) {
			Set messageVOCacheCopy = new HashSet();
			for (Iterator<Entry<String, Set<MessageVO>>> iterator = messageVOCache
					.entrySet().iterator(); iterator.hasNext();) {
				Entry<String, Set<MessageVO>> entry = iterator.next();
				messageVOCacheCopy.addAll(entry.getValue()); // 拷贝数据
			}
			messageVOCache.clear(); // 清空数据
			messageHistoryDao.insertMessageHistories(messageVOCacheCopy); // 提交数据
		}
	}

	class DispatcherGroupMessageVOThread implements Runnable {
		@Override
		public void run() {
			dispatcherGroupMessageVos();
		}

	}

	/**
	 * 获取所有的缓存数据 <br>
	 * 2013-6-26 下午10:42:46
	 * 
	 * @return
	 */
	public synchronized Set<MessageVO> getAllChaches() {
		Set<MessageVO> messageVOs = new LinkedHashSet<MessageVO>();
		for (Iterator<Entry<String, Set<MessageVO>>> iterator = groupMessageVOCache
				.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Set<MessageVO>> entry = iterator.next();
			messageVOs.addAll(entry.getValue()); // 拷贝数据
		}
		for (Iterator<Entry<String, Set<MessageVO>>> iterator = groupMessageVOCache
				.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Set<MessageVO>> entry = iterator.next();
			messageVOs.addAll(entry.getValue()); // 拷贝数据
		}
		return messageVOs;
	}

	/**
	 * 提交消息缓存数据到数据库
	 */
	public void dispatcherGroupMessageVos() {
		if (groupMessageVOCache == null || groupMessageVOCache.size() == 0) {
			return;
		}
		synchronized (LOCK) {
			Set groupMessageVOCacheCopy = new HashSet();
			for (Iterator<Entry<String, Set<MessageVO>>> iterator = groupMessageVOCache
					.entrySet().iterator(); iterator.hasNext();) {
				Entry<String, Set<MessageVO>> entry = iterator.next();
				groupMessageVOCacheCopy.addAll(entry.getValue()); // 拷贝数据
			}
			// groupMessageVOCacheCopy.addAll(messageVOCache); // 拷贝数据
			groupMessageVOCache.clear(); // 清空数据
			messageHistoryDao.insertMessageHistories(groupMessageVOCacheCopy); // 提交数据
		}
	}

	/**
	 * 提交数据线程
	 * 
	 * @author XiongYingqi
	 * 
	 */
	class DispatcherMessageHistoryCacheTimerTask extends TimerTask {
		@Override
		public void run() {
			MessageHandler.this.dispatcherMessageVOsInThread();
			MessageHandler.this.dispatcherGroupMessageVOsInThread();
		}
	}

	/**
	 * HashMapCache<String,LinkedHashSet>
	 * 
	 * @return the messageVOCache
	 */
	public HashMapCache<String, Set<MessageVO>> getMessageVOCache() {
		return messageVOCache;
	}

	/**
	 * HashMapCache<String,LinkedHashSet>
	 * 
	 * @return the groupMessageVOCache
	 */
	public HashMapCache<String, Set<MessageVO>> getGroupMessageVOCache() {
		return groupMessageVOCache;
	}

	/**
	 * 在程序结束前提交数据 <br>
	 * 2013-6-26 下午10:33:48
	 * 
	 * @see com.xiongyingqi.cache.CacheListener#dispose(com.xiongyingqi.cache.Cache)
	 */
	@Override
	public void dispose(Cache cache) throws CacheException {
		Collection messageVOs = new LinkedHashSet();
		for (Iterator<Entry<String, Set<MessageVO>>> iterator = messageVOCache
				.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Set<MessageVO>> entry = iterator.next();
			messageVOs.addAll(entry.getValue()); // 拷贝数据
		}
		for (Iterator<Entry<String, Set<MessageVO>>> iterator = groupMessageVOCache
				.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Set<MessageVO>> entry = iterator.next();
			messageVOs.addAll(entry.getValue()); // 拷贝数据
		}

		// messageVOs.addAll(groupMessageVOCache.entrySet());
		messageHistoryDao.insertMessageHistories(messageVOs);
	}

}
