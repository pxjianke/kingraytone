/**
 * spark_src
 */
package com.xiongyingqi.cache;

/**
 * @author XiongYingqi
 * @version 2013-6-26 下午10:07:37
 */
public interface Cache {
	public void notifyCacheDispose();
}
