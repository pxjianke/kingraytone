package com.xiongyingqi.cache;
/**
 * 缓存监听接口
 * @author xiongyingqi
 *
 */
public interface CacheListener {
	/**
	 * 当缓存结束时将会提醒实现本接口
	 * @param cache 结束的cache
	 * @throws CacheException
	 */
	public void dispose(Cache cache) throws CacheException;
}
