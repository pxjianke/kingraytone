/**
 * spark_src
 */
package com.xiongyingqi.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * @author XiongYingqi
 * @version 2013-6-24 下午10:47:33
 */
public class FileHelper {
	public static void copyFile(String originFilePath, String targetFilePath) {
		File originFile = new File(originFilePath);
		File targetFile = new File(targetFilePath);
		copyFile(originFile, targetFile);
	}

	public static void copyFile(URL originURL, File targetFile) {
		try {
			copyFile(originURL.openStream(), targetFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void copyFile(InputStream inputStream, File targetFile) {
		if (inputStream == null) {
			return;
		}
		if (!targetFile.getParentFile().exists()) {
			targetFile.getParentFile().mkdirs();
		}
		if (!targetFile.exists()) {
			try {
				targetFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(targetFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		byte[] buffer = new byte[1024 * 1024]; // 1M的缓存
		try {
			int length = inputStream.read(buffer);
			while (length > 0) {
				byte[] targetBuffer = new byte[length];
				System.arraycopy(buffer, 0, targetBuffer, 0, length);
				outputStream.write(targetBuffer);
				length = inputStream.read(buffer);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				outputStream.flush();
				outputStream.close();
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void copyFile(File originFile, File targetFile) {

		if (originFile.length() == targetFile.length()) {
			return;
		}
		if (!targetFile.exists()) {
			try {
				targetFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			inputStream = new FileInputStream(originFile);

			outputStream = new FileOutputStream(targetFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		System.out.println("Copy file: " + originFile);
		System.out.println("Target file: " + targetFile);
		byte[] buffer = new byte[1024 * 1024]; // 1M的缓存
		try {
			int length = inputStream.read(buffer);
			while (length > 0) {
				byte[] targetBuffer = new byte[length];
				System.arraycopy(buffer, 0, targetBuffer, 0, length);
				outputStream.write(targetBuffer);
				length = inputStream.read(buffer);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				outputStream.flush();
				outputStream.close();
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * 检查文件不为空且存在 <br>
	 * 2013-6-24 下午10:52:54
	 * 
	 * @param file
	 * @return
	 */
	public static boolean fileNotFullAndExists(File file) {
		return file != null && file.exists();
	}

}
