/**
 * spark_src
 */
package com.xiongyingqi.util;

import com.kingray.spark.plugin.KingrayPlugin;
import com.kingray.spark.plugin.vo.MessageVO;
import com.kingray.spark.plugin.vo.UserVO;

/**
 * @author XiongYingqi
 * @version 2013-6-26 下午10:23:55
 */
public class MessageVOHelper {

	/**
	 * 获取关联用户
	 * <br>2013-6-26 下午10:26:54
	 * @param messageVO
	 * @return
	 */
	public static String getRelationUserName(MessageVO messageVO){
		String relationUserName = null;
		UserVO messageFrom = messageVO.getMessageFrom();
		UserVO messageTo = messageVO.getMessageTo();
		if(messageFrom.getUserName().equals(KingrayPlugin.currentUserName)){
			relationUserName = messageTo.getUserName();
		} else {
			relationUserName = messageFrom.getUserName();
		}
		return relationUserName;
	}
}
