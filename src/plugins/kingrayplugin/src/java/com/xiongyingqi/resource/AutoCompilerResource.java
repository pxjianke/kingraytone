/**
 * spark_src
 */
package com.xiongyingqi.resource;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import org.jivesoftware.spark.util.log.Log;
/**
 * @author XiongYingqi
 * @version 2013-6-24 下午8:07:57
 */
public class AutoCompilerResource {
	public static void compile(){
		JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = javaCompiler.getStandardFileManager(null, null, null);
		String fileName = System.getProperty("user.dir") + "/src/plugins/kingrayplugin/src/java/com/xiongyingqi/resource/Resource.java";
		Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjects(fileName);
		
		for (JavaFileObject javaFileObject : compilationUnits) {
			String fileContent = "";
			Reader reader = null;
			try {
				reader = new FileReader(javaFileObject.toString());
				BufferedReader bufferedReader = new BufferedReader(reader);
				StringBuilder stringBuilder = new StringBuilder();
				String line = null;
				while ((line = bufferedReader.readLine()) != null) {
					stringBuilder.append(line + "\r\n");
				}
				fileContent = stringBuilder.toString();
			} catch (IOException e1) {
				e1.printStackTrace();
			} finally {
				if(reader != null){
					try {
						reader.close();
					} catch (IOException e) {
						Log.error(e);
					}
				}
			}
			
			Writer writer = null;
			try {
				writer = javaFileObject.openWriter();
				
				
				int braceIndex = searchBraceIndex(fileContent);
				StringBuilder frontContent = new StringBuilder(fileContent.substring(0, braceIndex + 1)); // 截取括号之前的文件内容
				frontContent.append("\r\n");
				
				StringBuilder behindContent = new StringBuilder(fileContent.substring(braceIndex + 1)); // 截取括号之后的文件内容
				
				File[] files = listFiles();
				for (int i = 0; i < files.length; i++) {
					File file = files[i];
					String resourceName = file.getName();
					resourceName = resourceName.substring(0, resourceName.lastIndexOf("."));
					String filePath = file.getAbsolutePath().replaceAll("\\\\", "/");
					String fieldName = "	public static final String " + resourceName.toUpperCase() + " = \"" + filePath  + "\"; ";
					if(!fileContent.contains(fieldName)){
						frontContent.append(fieldName);
						frontContent.append("\r\n");
					}
				}
				writer.write(frontContent.append(behindContent).toString());
			} catch (IOException e) {
				Log.error(e);
			} finally {
				try {
					writer.flush();
					writer.close();
				} catch (IOException e) {
					Log.error(e);
				}
			}
		}
		
		JavaCompiler.CompilationTask compilationTask = javaCompiler.getTask(null, fileManager, null, null, null, compilationUnits);
		compilationTask.call();
	}
	
	public static File[] listFiles(){
		String path = Resource.class.getResource(".").getPath();
		File pathFile = new  File(path);
		FilenameFilter filenameFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
//				System.out.println(name);
				if(name.endsWith("class")){
					return false;
				}
				return true;
			}
		};
		File[] files = pathFile.listFiles(filenameFilter);
		for (int i = 0; i < files.length; i++) {
			File resource = files[i];
//			System.out.println(resource.getName());
		}
		return files;
	}
	
	public static int searchBraceIndex(String fileString){
		return fileString.indexOf("{");
	}
	
	public static void main(String[] args) {
//		try {
//			String filePath = new File("").getCanonicalPath().replaceAll("\\\\", "/");
//			System.out.println(filePath);
//		} catch (IOException e) {
//			Log.error(e);
//		}
		
		AutoCompilerResource.compile();
//		Resource resource = new Resource();
//		Scanner scanner = new Scanner(System.in);
//		String content = scanner.next();
//		System.out.println(searchBraceIndex("asfasfasf{"));
	}

}
