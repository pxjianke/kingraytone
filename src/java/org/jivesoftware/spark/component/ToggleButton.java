/**
 * spark_src
 */
package org.jivesoftware.spark.component;

import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JToggleButton;

import org.jivesoftware.Spark;

/**
 * 
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-4 下午12:00:33
 */
public class ToggleButton extends JToggleButton {

	/**
	 * Create a new RolloverButton.
	 */
	public ToggleButton() {
		decorate();
	}

	public ToggleButton(String text) {
		super(text);
		decorate();
	}

	public ToggleButton(Action action) {
		super(action);
		decorate();
	}

	public ToggleButton(String text, Icon icon, boolean selected) {
		super(text, icon, selected);
		decorate();
	}
	public ToggleButton(String text, boolean selected) {
		super(text, selected);
		decorate();
	}
	public ToggleButton(Icon icon, boolean selected) {
		super(icon, selected);
		decorate();
	}
	/**
	 * Create a new RolloverButton.
	 * 
	 * @param icon
	 *            the icon to use on the button.
	 */
	public ToggleButton(Icon icon) {
		super(icon);
		decorate();
	}

	/**
	 * Create a new RolloverButton.
	 * 
	 * @param text
	 *            the button text.
	 * @param icon
	 *            the button icon.
	 */
	public ToggleButton(String text, Icon icon) {
		super(text, icon);
		decorate();
	}

	/**
	 * Decorates the button with the approriate UI configurations.
	 */
	protected void decorate() {
		setBorderPainted(false);
		setOpaque(true);

		setContentAreaFilled(false);
		setMargin(new Insets(1, 1, 1, 1));

		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				if (isEnabled()) {
					setBorderPainted(true);

					// Handle background border on mac.
					if (!Spark.isMac()) {
						setContentAreaFilled(true);
					}
				}
			}

			public void mouseExited(MouseEvent e) {
				setBorderPainted(false);
				setContentAreaFilled(false);
			}
		});

	}
}
