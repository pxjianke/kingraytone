/**
 * $RCSfile: ,v $
 * $Revision: $
 * $Date: $
 * 
 * Copyright (C) 2004-2011 Jive Software. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jivesoftware.spark.ui;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.sparkimpl.settings.local.SettingsManager;

import com.xiongyingqi.emotion.Emotion;
import com.xiongyingqi.ui.component.EmotionImageLabel;
import com.xiongyingqi.ui.component.ImageLabel;
import com.xiongyingqi.ui.component.PlayImageLabel;
import com.xiongyingqi.ui.component.editor.EditorComponent;
import com.xiongyingqi.ui.component.editor.EditorComponentEmotion;
import com.xiongyingqi.ui.component.editor.EditorComponentIcon;
import com.xiongyingqi.ui.component.editor.EditorComponentString;
import com.xiongyingqi.ui.component.editor.ClipBoarderHTMLParser;
import com.xiongyingqi.ui.component.editor.IconPermissionException;
import com.xiongyingqi.ui.component.editor.InputEditor;
import com.xiongyingqi.util.EntityHelper;
import com.xiongyingqi.utils.ClipBoardHelper;
import com.xiongyingqi.utils.FileHelper;
import com.xiongyingqi.utils.ClipBoardHelper.ClipBoardType;

/**
 * 聊天输入框
 * This is implementation of ChatArea that should be used as the sendField in
 * any chat room implementation.
 */
public class ChatInputEditor extends ChatArea implements DocumentListener, InputEditor {
	private static final long serialVersionUID = -3085035737908538581L;
	private final UndoManager undoManager;
	private KeyStroke undoKeyStroke;
	private KeyStroke ctrlbackspaceKeyStroke;
	private KeyStroke escapeKeyStroke;

	/**
	 * Creates a new Default ChatSendField.
	 */
	public ChatInputEditor() {
		undoManager = new UndoManager();

		this.setDragEnabled(true);
		this.getDocument().addUndoableEditListener(undoManager);
		Action undo = new AbstractAction() {
			private static final long serialVersionUID = -8897769620508545403L;

			public void actionPerformed(ActionEvent e) {
				try {
					undoManager.undo();
				} catch (CannotUndoException cue) {
					// no more undoing for you
				}
			}
		};

		Action escape = new AbstractAction() {
			private static final long serialVersionUID = -2973535045376312313L;

			@Override
			public void actionPerformed(ActionEvent e) {
				SparkManager.getChatManager().getChatContainer().closeActiveRoom();
			}
		};

		Action ctrlbackspace = new AbstractAction() {
			private static final long serialVersionUID = -2973535045376312313L;

			@Override
			public void actionPerformed(ActionEvent e) {

				// We have Text selected, remove it
				if (getSelectedText() != null && getSelectedText().length() > 0) {
					ChatInputEditor.this
							.removeWordInBetween(getSelectionStart(), getSelectionEnd());

					// We are somewhere in betwee 0 and str.length
				} else if (getCaretPosition() < getText().length()) {

					String preCaret = getText().substring(0, getCaretPosition());

					int lastSpace = preCaret.lastIndexOf(" ") != -1 ? preCaret.lastIndexOf(" ") : 0;

					if (lastSpace != -1 && lastSpace != 0) {
						// Do we have anymore spaces before the current one?
						for (int i = lastSpace; getText().charAt(i) == ' '; --i) {
							lastSpace--;
						}
						lastSpace++;
					}
					ChatInputEditor.this.removeWordInBetween(lastSpace, getCaretPosition());

					if (lastSpace <= getText().length()) {
						setCaretPosition(lastSpace);
					} else {
						setCaretPosition(getText().length());
					}

					// We are at the end and will remove until the next SPACE
				} else if (getText().contains(" ")) {
					int untilhere = getText().lastIndexOf(" ");

					// Do we have anymore spaces before the last one?
					for (int i = untilhere; getText().charAt(i) == ' '; --i) {
						untilhere--;
					}
					untilhere++;
					ChatInputEditor.this.removeLastWord(getText().substring(untilhere));
				} else {
					ChatInputEditor.this.removeLastWord(getText());
				}
			}
		};

		undoKeyStroke = KeyStroke.getKeyStroke('z', ActionEvent.CTRL_MASK);
		ctrlbackspaceKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, KeyEvent.CTRL_MASK);
		escapeKeyStroke = KeyStroke.getKeyStroke("ESCAPE");

		getInputMap().put(ctrlbackspaceKeyStroke, "ctrlbackspace");
		getInputMap().put(undoKeyStroke, "undo");
		getInputMap().put(escapeKeyStroke, "escape");
		getInputMap().put(KeyStroke.getKeyStroke("Ctrl W"), "escape");

		registerKeyboardAction(undo, KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_MASK),
				JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		registerKeyboardAction(ctrlbackspace,
				KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, KeyEvent.CTRL_MASK),
				JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		registerKeyboardAction(escape, KeyStroke.getKeyStroke("ESCAPE"),
				JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

		getDocument().addDocumentListener(this);

		addMouseListener(this);
	}

	public void insertUpdate(DocumentEvent e) {
		// this.setCaretPosition(e.getOffset());
		this.requestFocusInWindow();
	}

	/**
	 * Appends the Text at the end
	 */
	public void setText(String str) {
		super.setText(str);
	}

	public void removeUpdate(DocumentEvent e) {
	}

	public void changedUpdate(DocumentEvent e) {
	}

	/**
	 * Remove dependices when no longer in use.
	 */
	public void close() {
		getDocument().removeDocumentListener(this);
		getDocument().removeUndoableEditListener(undoManager);
		removeMouseListener(this);
		getInputMap().remove(undoKeyStroke);
		getInputMap().remove(ctrlbackspaceKeyStroke);
		getInputMap().remove(escapeKeyStroke);
		getInputMap().remove(KeyStroke.getKeyStroke("Ctrl W"));
	}

	/**
	 * Disables the Chat Editor, rendering it to the system default color.
	 */
	public void showAsDisabled() {
		this.setEditable(false);
		this.setEnabled(false);

		clear();

		final Color disabledColor = (Color) UIManager.get("Button.disabled");

		this.setBackground(disabledColor);
	}

	/**
	 * Enable the Chat Editor.
	 */
	public void showEnabled() {
		this.setEditable(true);
		this.setEnabled(true);

		this.setBackground(Color.white);
	}

	/**
	 * Inserts text into the current document.
	 * 
	 * @param text
	 *            the text to insert
	 * @throws BadLocationException
	 *             if the location is not available for insertion.
	 */
	@Override
	public void insertText(String text) throws BadLocationException {
		final Document doc = getDocument();
		styles.removeAttribute("link");
		doc.insertString(this.getCaret().getDot(), text, styles);
	}

	/**
	 * Inserts text into the current document.
	 * 
	 * @param text
	 *            the text to insert
	 * @param color
	 *            the color of the text
	 * @throws BadLocationException
	 *             if the location is not available for insertion.
	 */
	@Override
	public void insertText(String text, Color color) throws BadLocationException {
		final Document doc = getDocument();
		StyleConstants.setForeground(styles, color);
		doc.insertString(this.getCaret().getDot(), text, styles);
	}

	/**
	 * Inserts a link into the current document.
	 * 
	 * @param link
	 *            - the link to insert( ex. http://www.javasoft.com )
	 * @throws BadLocationException
	 *             if the location is not available for insertion.
	 */
	@Override
	public void insertLink(String link) throws BadLocationException {
		final Document doc = getDocument();
		styles.addAttribute("link", link);

		StyleConstants.setForeground(styles, (Color) UIManager.get("Link.foreground"));
		StyleConstants.setUnderline(styles, true);
		doc.insertString(this.getCaret().getDot(), link, styles);
		StyleConstants.setUnderline(styles, false);
		StyleConstants.setForeground(styles, (Color) UIManager.get("TextPane.foreground"));
		styles.removeAttribute("link");
		setCharacterAttributes(styles, false);

	}

	/**
	 * Inserts a network address into the current document.
	 * 
	 * @param address
	 *            - the address to insert( ex. \superpc\etc\file\ OR
	 *            http://localhost/ )
	 * @throws BadLocationException
	 *             if the location is not available for insertion.
	 */
	@Override
	public void insertAddress(String address) throws BadLocationException {
		final Document doc = getDocument();
		styles.addAttribute("link", address);

		StyleConstants.setForeground(styles, (Color) UIManager.get("Address.foreground"));
		StyleConstants.setUnderline(styles, true);
		doc.insertString(this.getCaret().getDot(), address, styles);
		StyleConstants.setUnderline(styles, false);
		StyleConstants.setForeground(styles, (Color) UIManager.get("TextPane.foreground"));
		styles.removeAttribute("link");
		setCharacterAttributes(styles, false);
	}
	
	/**
	 * Inserts an emotion icon into the current document.
	 * 
	 * @param imageKey
	 *            - the smiley representation of the image.( ex. :) )
	 * @return true if the image was found, otherwise false.
	 */
	@Override
	public boolean insertImage(String imageKey) {

		if (!forceEmoticons && !SettingsManager.getLocalPreferences().areEmoticonsEnabled()
				|| !emoticonsAvailable) {
			return false;
		}
		final Document doc = getDocument();
		Icon emotion = emoticonManager.getEmoticonImage(imageKey.toLowerCase());
		if (emotion == null) {
			return false;
		}

		select(doc.getLength(), doc.getLength());
		insertIcon(emotion);

		return true;
	}
	
	/**
	 * <br>2013-9-4 下午2:52:55
	 * @see javax.swing.JEditorPane#getText()
	 */
	@Override
	public String getText() {
		String content = buildContentToString();
		if(content != null){
			return content;
		}
		return super.getText();
	}
	/**
	 * <br>2013-9-4 下午2:47:46
	 * @see org.jivesoftware.spark.ui.ChatArea#pasteAction()
	 */
	@Override
	protected void pasteAction() {
		processClipBoard();
	}
	
	/**
	 * 插入字符串
	 * <br>2013-9-4 上午10:53:02
	 * @param text
	 */
	public void insertString(String text) {
		// this.setCaretPosition(document.getLength());
		try {
			StyledDocument document = this.getStyledDocument();
			int selectionStart = this.getSelectionStart();
			int selectionEnd = this.getSelectionEnd();
			if(selectionStart > selectionEnd){ // 如果开始选择的位置大于结束的位置
				selectionStart = selectionStart + selectionEnd;// selectionStart变为两个变量的并集
				selectionEnd = selectionStart - selectionEnd;// selectionEnd为并集减去原始的selectionEnd值
				selectionStart = selectionStart - selectionEnd;// 剩下的则为原来的selectionEnd
			}
			document.remove(selectionStart, selectionEnd - selectionStart);// 移除选择的文本
			this.setCaretPosition(selectionStart);
			
			int position = this.getCaretPosition();
			document.insertString(position, text, null);
			this.setCaretPosition(position + text.length());
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 处理剪切板中的内容
	 * <br>2013-9-4 上午10:45:53
	 */
	public void processClipBoard(){
		Map<Object, ClipBoardType> map = ClipBoardHelper.getClipBoardTypeMap();
		Set<Entry<Object, ClipBoardType>> entries = map.entrySet();
		
		List<Image> images = new ArrayList<Image>(); // 用于保存html，当以html的方式读取失败后将试图以image方式再次读图片
		List<File> files = new ArrayList<File>(); // 用于保存html，当以html的方式读取失败后将试图以image方式再次读图片
		for (Iterator iterator = entries.iterator(); iterator.hasNext();) {// 预读image信息
			Entry<Object, ClipBoardType> entry = (Entry<Object, ClipBoardType>) iterator.next();
			Object object = entry.getKey();
			ClipBoardType clipBoardType = entry.getValue();
			switch (clipBoardType) {
			case IMAGE:
				Image image = (Image) object;
				if(object instanceof BufferedImage){
					File file = FileHelper.readBufferImage((BufferedImage) image);
					files.add(file);
//					Icon icon = new ImageIcon(image);
//					TextPane.this.insertIcon(icon);
				}
				images.add(image);
				break;
			default:
				break;
			}
		}
		
		
		
		int k = 0;
		loopOut: for (Iterator iterator = entries.iterator(); iterator.hasNext();) {
			Entry<Object, ClipBoardType> entry = (Entry<Object, ClipBoardType>) iterator.next();
			Object object = entry.getKey();
			ClipBoardType clipBoardType = entry.getValue();
			switch (clipBoardType) {
			case HTML:
				EditorComponent[] components = ClipBoarderHTMLParser.parseHTML(object.toString());
				if (components != null) {
					for (int i = 0; i < components.length; i++) {
						EditorComponent editorComponent = components[i];
//						System.out.println("editorComponent ======== " + editorComponent);
						if (editorComponent != null) {
							try {
								editorComponent.insertContentToTextPane(this);
							} catch (Exception e) {
//								e.printStackTrace();
								if (e instanceof IconPermissionException) {
//									Icon icon = new ImageIcon(images.get(k));
//									this.insertIcon(icon);
									if(files.size() > 0){
										File file = files.get(k);
										k++;
										editorComponent.setContent(file);
										try {
											editorComponent.insertContentToTextPane(this);
										} catch (Exception e1) {
										}
									} else if(images.size() > 0) {
										Image image = images.get(k);
										k++;
										Icon icon = new ImageIcon(image);
										this.insertIcon(icon);
									} else {
										continue loopOut;
									}
								}
							}
						}
					}
				}
				break loopOut;// 如果是html类型，则不需要
			case IMAGE:
				// Icon icon = (Icon) object;
				Image image = (Image) object;
				Icon icon = new ImageIcon(image);
				this.insertIcon(icon);

				// Style labelStyle =
				// context.getStyle(StyleContext.DEFAULT_STYLE);
				//
				// JLabel label = new JLabel(icon);
				// StyleConstants.setComponent(labelStyle, label);
				//
				// try {
				// document.insertString(document.getLength(), "Ignored",
				// labelStyle);
				// } catch (BadLocationException badLocationException) {
				// System.err.println("Oops");
				// }
				// MediaTracker mediaTracker = new
				// MediaTracker(EditorPanel.this);
				// mediaTracker.addImage(image, 0);
				// try {
				// mediaTracker.waitForID(0);
				// } catch (InterruptedException e) {
				// e.printStackTrace();
				// }
				break;
			case STRING:
				this.insertString(object.toString());
				break;
			// case STRINGREADER:
			// TextPane.this.insertString(TextPane.this.stringReaderToString((StringReader)object));
			// break;
			case FILE:
				List<File> list = (List<File>) object;
				for (Iterator iterator2 = list.iterator(); iterator2.hasNext();) {
					File file = (File) iterator2.next();
					String fileName = file.getName();
					System.out.println(fileName);
				}
				break;
//			 case INPUT_STREAM:
//				 InputStream inputStream = (InputStream) object;
//				 File file = FileHelper.readInputStream(inputStream);
//				 EntityHelper.print(file);
//			 break;
			default:
				break;
			}
		}
	}
	
	/**
	 * 插入图标
	 * <br>2013-9-4 上午10:52:54
	 * @see javax.swing.JTextPane#insertIcon(javax.swing.Icon)
	 */
	@Override
	public void insertIcon(Icon icon) {
		// StyledDocument document = TextPane.this.getStyledDocument();
		StyledDocument document = this.getStyledDocument();
		int selectionStart = this.getSelectionStart();
		int selectionEnd = this.getSelectionEnd();
		if(selectionStart > selectionEnd){ // 如果开始选择的位置大于结束的位置
			selectionStart = selectionStart + selectionEnd;// selectionStart变为两个变量的并集
			selectionEnd = selectionStart - selectionEnd;// selectionEnd为并集减去原始的selectionEnd值
			selectionStart = selectionStart - selectionEnd;// 剩下的则为原来的selectionEnd
		}
		try {
			document.remove(selectionStart, selectionEnd - selectionStart);// 移除选择的文本
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		this.setCaretPosition(selectionStart);
		
		int position = this.getCaretPosition();
		super.insertIcon(icon);
		this.setCaretPosition(position + 1);
	}
	
	
	/**
	 * 将内容转换为字符串
	 * <br>2013-9-4 下午2:55:54
	 * @return
	 */
	private String buildContentToString(){
		StringBuilder builder = new StringBuilder();
		Collection<EditorComponent> components = buildContent();
		for (Iterator iterator = components.iterator(); iterator.hasNext();) {
			EditorComponent editorComponent = (EditorComponent) iterator.next();
			try {
				String encodedString = editorComponent.encodeToString();
				if(encodedString != null){
					builder.append(encodedString);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		String rs = builder.toString();
		return rs;
	}
	/**
	 * 将编辑内容转换成EditorComponent集合 <br>
	 * 2013-9-3 上午11:39:43
	 * 
	 * @return
	 */
	public Collection<EditorComponent> buildContent() {
		StyledDocument document = this.getStyledDocument();
		Element element = document.getDefaultRootElement();

		imageLabels.clear();
		iterateElements(element);

		Collection<EditorComponent> components = new ArrayList<EditorComponent>();

		String content = super.getText();
		int length = content.length();
		int k = 0;// 当前从图片集合取值的便宜值

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < length; i++) {
			Element characterElement = document.getCharacterElement(i);
			String name = characterElement.getName();
			// System.out.println(name);
			// Object unit = null;
//			if (name.equals("icon")) {
//				Icon icon = icons.get(k);
//				
//				// 如果为图片类型，则包装之前组装的字符串
//				String str = builder.toString();
//				EditorComponent componentString = new EditorComponentString();
//				componentString.setContent(str);
//				components.add(componentString);
//				builder = new StringBuilder();// 重置字符串
//				
//				// 包装图片类型
//				String urlString = icon.toString();
//				if(icon instanceof BufferedImage){
//					BufferedImage image = (BufferedImage) icon;
//					File file = FileHelper.readBufferImage(image);
//					urlString = FileHelper.toURL(file).toString();
//				}
//				try {
//					urlString = URLDecoder.decode(urlString, "UTF-8");// 解码
//				} catch (UnsupportedEncodingException e1) {
//					e1.printStackTrace();
//				}
//				System.out.println("urlString ========== " + urlString);
//				try {
//					File iconFile = FileHelper.readURL(urlString);
//					EditorComponent componentIcon = new EditorComponentIcon();
//					componentIcon.setContent(iconFile);
//					components.add(componentIcon);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//				
//				k++;// 进入下一个图片
			if (name.equals("component")) {
				ImageLabel imageLabel = imageLabels.get(k);

				// 如果为图片类型，则包装之前组装的字符串
				String str = builder.toString();
				EditorComponent componentString = new EditorComponentString();
				componentString.setContent(str);
				components.add(componentString);
				builder = new StringBuilder();// 重置字符串
				
				// 包装图片类型
				String urlString = FileHelper.toURL(imageLabel.getIconFile()).toString();
				try {
					urlString = URLDecoder.decode(urlString, "UTF-8");// 解码
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
				try {
					File iconFile = FileHelper.readURL(urlString);
					if(imageLabel instanceof PlayImageLabel){
						EditorComponent componentIcon = new EditorComponentIcon();
						componentIcon.setContent(iconFile);
						components.add(componentIcon);
					} else if(imageLabel instanceof EmotionImageLabel){
						EditorComponent componentIcon = new EditorComponentEmotion();
						EmotionImageLabel emotionImageLabel = (EmotionImageLabel) imageLabel;
						componentIcon.setContent(emotionImageLabel.getEmotion());
						components.add(componentIcon);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

				k++;// 进入下一个图片
			} else if (name.equals("content")) {
				try {
					String str = document.getText(i, 1);
					if (str != null) {
						builder.append(str);
					}
				} catch (BadLocationException e1) {
					// e1.printStackTrace();
				}
			}
		}
		String str = builder.toString();
		EditorComponent componentString = new EditorComponentString();// 如果最后一个不是插入图片，则封装最后一个图片
		componentString.setContent(str);
		
		components.add(componentString);
		return components;
	}

//	private Vector<Icon> icons = new Vector<Icon>();
	private Vector<ImageLabel> imageLabels = new Vector<ImageLabel>();
	/**
	 * 迭代所有的图片对象 <br>
	 * 2013-9-3 下午3:01:15
	 * 
	 * @param element
	 */
	public void iterateElements(Element element) {
		AttributeSet attributeSet = element.getAttributes();
		Enumeration<?> enumeration = attributeSet.getAttributeNames();
		while (enumeration.hasMoreElements()) {
			Object key = (Object) enumeration.nextElement();
			Object value = attributeSet.getAttribute(key);
			System.out.println("key: " + key + " value: " + value);
			
//			if (value instanceof Icon) {
////				icons.add((Icon) value);
////				Icon icon = StyleConstants.getIcon(attributeSet);
//				ImageIcon icon = (ImageIcon) value;
//				icons.add(icon);
//				ImageProducer imageProducer = icon.getImage().getSource();
//			}
			
			if(value instanceof ImageLabel){
				ImageLabel imageLabel = (ImageLabel) value;
				imageLabels.add(imageLabel);
			}
			
			// if(value instanceof NamedStyle){
			// NamedStyle namedStyle = (NamedStyle) value;
			// Enumeration<?> enumeration2 = namedStyle.getAttributeNames();
			// while (enumeration2.hasMoreElements()) {
			// Object key2 = (Object) enumeration2.nextElement();
			// Object value2 = namedStyle.getAttribute(key2);
			// System.out.println("key2: " + key2 + " ----- value2: " + value2);
			// }
			// }
		}
		int size = element.getElementCount();
		for (int i = 0; i < size; i++) {
			iterateElements(element.getElement(i));
		}
	}

	/**
	 * <br>2013-9-4 下午3:27:45
	 * @see com.xiongyingqi.ui.component.editor.InputEditor#insertIcon(java.io.File)
	 */
	@Override
	public void insertIcon(File iconFile) {
		StyledDocument document = this.getStyledDocument();
		int selectionStart = this.getSelectionStart();
		int selectionEnd = this.getSelectionEnd();
		if(selectionStart > selectionEnd){ // 如果开始选择的位置大于结束的位置
			selectionStart = selectionStart + selectionEnd;// selectionStart变为两个变量的并集
			selectionEnd = selectionStart - selectionEnd;// selectionEnd为并集减去原始的selectionEnd值
			selectionStart = selectionStart - selectionEnd;// 剩下的则为原来的selectionEnd
		}
		try {
			document.remove(selectionStart, selectionEnd - selectionStart);// 移除选择的文本
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		this.setCaretPosition(selectionStart);
		
		int position = this.getCaretPosition();
		PlayImageLabel imageLabel = new PlayImageLabel(iconFile);
		super.insertComponent(imageLabel);
//		super.insertIcon(icon);
		this.setCaretPosition(position + 1);
	}

	/**
	 * <br>2013-9-4 下午3:27:45
	 * @see com.xiongyingqi.ui.component.editor.InputEditor#insertEmotion(java.lang.String)
	 */
	@Override
	public void insertEmotion(Emotion emotion) {
		StyledDocument document = this.getStyledDocument();
		int selectionStart = this.getSelectionStart();
		int selectionEnd = this.getSelectionEnd();
		if(selectionStart > selectionEnd){ // 如果开始选择的位置大于结束的位置
			selectionStart = selectionStart + selectionEnd;// selectionStart变为两个变量的并集
			selectionEnd = selectionStart - selectionEnd;// selectionEnd为并集减去原始的selectionEnd值
			selectionStart = selectionStart - selectionEnd;// 剩下的则为原来的selectionEnd
		}
		try {
			document.remove(selectionStart, selectionEnd - selectionStart);// 移除选择的文本
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		this.setCaretPosition(selectionStart);
		
		int position = this.getCaretPosition();
		EmotionImageLabel imageLabel = new EmotionImageLabel(emotion);
		super.insertComponent(imageLabel);
//		super.insertIcon(icon);
		this.setCaretPosition(position + 1);
	}


}