/**
 * spark_src
 */
package com.xiongyingqi.ui;

import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JDialog;

import java.awt.Color;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;

import org.jivesoftware.resource.Default;
import org.jivesoftware.resource.SparkRes;
import org.jivesoftware.spark.component.BackgroundPanel;

import com.xiongyingqi.resources.Resource;

/**
 * 安装界面
 * 
 * @author XiongYingqi
 * @version 2013-6-25 上午9:27:53
 */
public class InstallerChromeFrame extends JDialog {
	private ImageIcon image = SparkRes.getImageIcon(SparkRes.LOADING); // Resource.getImageResourcePath("loading.gif")
	private static InstallerChromeFrame singleton;

	public static InstallerChromeFrame getInstance(Frame owner) {
		if (singleton == null) {
			singleton = new InstallerChromeFrame(owner);
		}
		return singleton;
	}

	private InstallerChromeFrame(Frame owner) {
		super(owner, "安装提示", true);
		this.setSize(300, 190);
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel titlePanel = new JPanel();
		titlePanel.setBackground(Color.WHITE);
		getContentPane().add(titlePanel, BorderLayout.NORTH);

		JLabel lblchrome = new JLabel("您没有安装Chrome浏览器，正在为您安装...");
		titlePanel.add(lblchrome);

		JPanel flashPanel = new MyImagePanel();
		flashPanel.setBackground(Color.WHITE);
		getContentPane().add(flashPanel, BorderLayout.CENTER);
		Toolkit tk = Toolkit.getDefaultToolkit();
		setLocation((tk.getScreenSize().width - this.getSize().width) / 2,
				(tk.getScreenSize().height - this.getSize().height) / 2); // 居中
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		final JPanel topPanel = new BackgroundPanel();
		topPanel.setLayout(new GridBagLayout());
	}

	class MyImagePanel extends JPanel {
		// private ImageIcon image = new ImageIcon(
		// "D:/workspace_JavaSE/spark_src/src/plugins/kingrayplugin/src/resources/images/loading.gif");

		MyImagePanel() {
			super(true);
			JLabel imageLabel = new JLabel();
			System.out.println("image =========== " + image);
			imageLabel.setBounds(this.getWidth() / 2 - image.getIconWidth(),
					this.getHeight() / 2 - image.getIconHeight(),
					image.getIconWidth(), image.getIconHeight());
			this.add(imageLabel);
			imageLabel.setIcon(image);
		}
	}

	public static void main(String[] args) {
		InstallerChromeFrame.getInstance(null).setVisible(true);
	}
}
