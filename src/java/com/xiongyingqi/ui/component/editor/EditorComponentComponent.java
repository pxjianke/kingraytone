/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

import java.awt.Component;
import java.util.regex.Pattern;

import javax.swing.JComponent;
import javax.swing.JTextPane;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-2 下午4:04:59
 */
public class EditorComponentComponent extends AbstractEditorComponent {
	private static final Pattern PATTERN = Pattern.compile("<component[^<>]+>");
	/**
	 * @param priority
	 */
	public EditorComponentComponent() {
		super(1);
	}

	private Component component;

	/**
	 * <br>
	 * 2013-9-2 下午4:05:13
	 * 
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#getContent()
	 */
	@Override
	public Object getContent() {
		return component;
	}

	/**
	 * <br>
	 * 2013-9-2 下午4:05:13
	 * 
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#setContent(java.lang.Object)
	 */
	@Override
	public void setContent(Object content) {
		if (content instanceof Component) {
			this.component = (Component) content;
		} else {
			throw new ClassCastException("类型错误");
		}
	}

	/**
	 * <br>
	 * 2013-9-2 下午4:05:13
	 * 
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#insertContentToTextPane(javax.swing.JTextPane)
	 */
	@Override
	public void insertContentToTextPane(InputEditor inputEditor) throws Exception {
		inputEditor.insertComponent(component);
	}

	/**
	 * <br>
	 * 2013-9-2 下午4:05:13
	 * 
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#geType()
	 */
	@Override
	public EditorComponentType geType() {
		return EditorComponentType.COMPONENT;
	}

	/**
	 * <br>
	 * 2013-9-3 下午12:03:35
	 * 
	 * @throws Exception
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#encodeToString()
	 */
	@Override
	public String encodeToString() throws Exception {
		throw new Exception("未实现该方法！");
//		return null;
	}

	/**
	 * <br>
	 * 2013-9-3 下午12:03:35
	 * @throws Exception 
	 * 
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#decodeFromString(java.lang.String)
	 */
	@Override
	public EditorComponent decodeFromString(String encoded) throws EditorComponentSupportException {
		throw new EditorComponentSupportException("未实现该方法！");
//		return null;
	}

	/**
	 * <br>2013-9-18 上午11:13:01
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#getParsePattern()
	 */
	@Override
	public Pattern getParsePattern() throws EditorComponentSupportException {
		throw new EditorComponentSupportException("未实现该方法！");
	}

}
