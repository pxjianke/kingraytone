/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.xiongyingqi.utils.EntityHelper;
import com.xiongyingqi.utils.FileHelper;
import com.xiongyingqi.utils.StringHelper;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-8-30 下午5:10:20
 */
public class ClipBoarderHTMLParser {
	/**
	 * 获取标签信息
	 */
	private static final Pattern TAG_PATTERN = Pattern
			.compile("<[\\w]+[\\s]+?[^<>]+>");
	/**
	 * HTML标签的属性
	 */
	private static final Pattern TAG_PROPERTIES_PATTERN = Pattern
			.compile("[\\w]+[\\s]*[=]{1}[\\s]*(?:((?!')[\\\"].*?[\\\"])|((?!\\\")['].*?[']))");
	
	
	private static final Pattern START_FRAGMENT = Pattern
			.compile("<!--[\\s]*?startfragment\\s*?-->");// 去除样式信息，获取标签值
	private static final Pattern END_FRAGMENT = Pattern.compile("<!--[\\s]*?endfragment\\s*?-->");// 去除样式信息，获取标签值
	private static final Pattern PATTERN_SPACE = Pattern.compile("<[^<>]*?[\\s\\\\>]");// 去除样式信息，获取标签值
	private static final Pattern PATTERN = Pattern.compile("<[^<>]+>");// 捕获所有标签内容
	private static final Pattern IMAGE_SRC_PATTERN = Pattern.compile("src[\\s=]*[\"'].*?[\"']");// 捕获img标签的src地址
	private static final Pattern DIV_IMAGE_SRC_PATTERN = Pattern
			.compile("background-image:[\\s]*url\\u0028[\"']{0,1}.*?[\"']{0,1}\\u0029");// 捕获div标签的img地址
	private static final Pattern LINK_PATTERN = Pattern
			.compile("href[=\\s]*[\"'][http://]*[^\"]+[\"']");// a标记的地址


	public static EditorComponent[] parseHTML(String html) {
		EditorComponent[] components = null;
		if (html != null && !"".equals(html)) {
			html = parseFragment(html.toLowerCase());
			html = filterDecode(html);
			components = searchForTags(html);
		}
		return components;
	}

	/**
	 * 解释<Fragment>片段 <br>
	 * 2013-8-30 下午5:55:24
	 * 
	 * @return
	 */
	private static String parseFragment(String origin) {
		Matcher matcherStart = START_FRAGMENT.matcher(origin);
		Matcher matcherEnd = END_FRAGMENT.matcher(origin);
		int end = 0;
		int start = 0;
		while (matcherStart.find()) {
			end = matcherStart.end();// 找到StartFragment标签的结尾位置
		}
		while (matcherEnd.find()) {
			start = matcherEnd.start();// 找到EndFragment标签的开始位置
		}

		origin = origin.substring(end, start);

		// System.out.println(origin);
		// int start = origin.indexOf(startFragment);
		// int end = origin.indexOf(endFragment);
		// = origin.substring(start + startFragment.length(), end);
		// System.out.println(rs);
		System.out.println(origin);
		return origin;
	}

	/**
	 * 获取标签 <br>
	 * 2013-8-30 下午6:02:23
	 * 
	 * @return
	 */
	private static EditorComponent[] searchForTags(String origin) {
		// Pattern pattern = Pattern.compile("<[^>]+>[^<</>/>]*?</[^>]+?>");

		// StringBuilder builder = new StringBuilder();
		origin = origin.toLowerCase();

		List<EditorComponent> components = new ArrayList<EditorComponent>();

		Matcher m = PATTERN.matcher(origin);
		int startIndex = 0; // 匹配的开始位置
		int endIndex = 0; // 匹配的结束位置
		while (m.find()) {
			String s = m.group(); // 标签
			// System.out.println("s =========== " + s);

			startIndex = m.start(); // 获取当前匹配的开始位置

			String content = origin.substring(endIndex, startIndex);// 截取上一个标签的结束位置和当前标签开始位置的字符串（标签
																	// 内容）
			EditorComponent editorComponent = new EditorComponentString();// 嵌入字符串对象
			editorComponent.setContent(content);
			components.add(editorComponent);

			endIndex = m.end(); // 获取当前匹配的结束位置

			components.add(parseTag(s));
		}

		return components.toArray(new EditorComponent[] {});
	}

	/**
	 * 根据具体的标签值解析 <br>
	 * 2013-9-3 上午10:05:34
	 * 
	 * @param tag
	 * @return
	 */
	private static EditorComponent parseTag(String tag) {
		EditorComponent editorComponent = null;
		Matcher mSpace = PATTERN_SPACE.matcher(tag);
		while (mSpace.find()) {
			String spaceString = mSpace.group().replaceAll("<", "").replaceAll(">", "").trim();// 去除尖括号和空格
			if (spaceString.equals("p")) { // 如果是p标签
				String content = StringHelper.line() + StringHelper.line();
				editorComponent = new EditorComponentString();// 嵌入字符串对象
				editorComponent.setContent(content);
			} else if (spaceString.equals("img")) { // 如果是img标签
				Matcher matcher = IMAGE_SRC_PATTERN.matcher(tag);
				while (matcher.find()) {
					String url = matcher.group().replaceFirst("src=", "").trim();
					url = url.substring(1, url.length() - 1);
					File iconFile;
					editorComponent = new EditorComponentIcon();// 嵌入图片
					try {
						iconFile = FileHelper.readURL(url);
						String mimeType = FileHelper.getMimeType(iconFile);
						if (mimeType != null && mimeType.toLowerCase().startsWith("image")) {
							editorComponent.setContent(iconFile);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else if (spaceString.equals("div")) { // 如果是有背景图片的div
				Matcher matcher = DIV_IMAGE_SRC_PATTERN.matcher(tag);
				while (matcher.find()) {
					String url = matcher.group().replaceFirst("background-image:", "")
							.replaceFirst("url", "").replaceFirst("\\u0028", "").trim();
					int index = url.lastIndexOf("\u0029");
					if (index > 0) {
						url = url.substring(0, index);
					}

					File iconFile;
					try {
						iconFile = FileHelper.readURL(url);
						String mimeType = FileHelper.getMimeType(iconFile);
						if (mimeType.toLowerCase().startsWith("image")) {
							editorComponent = new EditorComponentIcon();// 嵌入字符串对象
							editorComponent.setContent(iconFile);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}

					// rs = url;
				}
			} else if (spaceString.equals("br")) {
				String content = StringHelper.line();
				editorComponent = new EditorComponentString();// 嵌入字符串对象
				editorComponent.setContent(content);
			} else if (spaceString.equals("a")) {
				Matcher matcher = LINK_PATTERN.matcher(tag);// href="http://www.xiongyingqi.com/"
				while (matcher.find()) {
					String url = matcher.group().replaceFirst("href", "").replaceAll("=", "")
							.replaceAll("\"", "").trim();
					if (!url.startsWith("http://")) {
						url += "http://";
					}
					url = " " + url + " ";// 补上空格
					editorComponent = new EditorComponentString();// 嵌入字符串对象
					editorComponent.setContent(url);
				}
			}
		}
		// System.out.println("rs ============= " + rs);
		return editorComponent;
	}

	public static String filterDecode(String url) {
		url = url.replaceAll("&amp;", "&").replaceAll("&lt;", "<").replaceAll("&gt;", ">")
				.replaceAll("&apos;", "\'").replaceAll("&quot;", "\"").replaceAll("&nbsp;", " ")
				.replaceAll("&copy;", "@").replaceAll("&reg;", "?");
		return url;
	}

	public static void main(String[] args) {
		String html = FileHelper.readFileToString("tmp/a.html");
		parseHTML(html);
	}
}
