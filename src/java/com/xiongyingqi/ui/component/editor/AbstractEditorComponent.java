/**
 * spark_src
 */
package com.xiongyingqi.ui.component.editor;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-18 上午10:09:03
 */
public abstract class AbstractEditorComponent implements EditorComponent {
	/**
	 * 获取标签信息
	 */
	protected static final Pattern TAG_PATTERN = Pattern.compile("<[\\w]+[\\s]+?[^<>]+>");
	/**
	 * HTML标签的属性
	 */
	protected static final Pattern TAG_PROPERTIES_PATTERN = Pattern
			.compile("[\\w]+[\\s]*[=]{1}[\\s]*(?:((?!')[\\\"].*?[\\\"])|((?!\\\")['].*?[']))");

	/**
	 * 获取属性的键<br>
	 */
	protected static final Pattern TAG_PROPERTIES_KEY_PATTERN = Pattern.compile("[^=\\s\"']+");

	/**
	 * 获取属性的值<br>
	 */
	protected static final Pattern TAG_PROPERTIES_VALUE_PATTERN = Pattern
			.compile("(?:((?!')[\\\"].*?[\\\"])|((?!\\\")['].*?[']))");

	private int priority;

	protected AbstractEditorComponent(int priority) {
		this.priority = priority;
	}

	/**
	 * int
	 * 
	 * @return the priority
	 */
	public int getDecodePriority() {
		return priority;
	}

	/**
	 * 根据html标准获取标签属性值，如
	 * 
	 * <pre>
	 * 	&lt;emotion id="asd" src="asfaaaaaaaaaaafffffffffffffff"&gt;
	 * </pre>
	 * 
	 * 则会返回 id:asd src:asfaaaaaaaaaaafffffffffffffff <br>
	 * 2013-9-18 下午6:45:59
	 * 
	 * @param tag
	 * @return
	 */
	protected Map<String, String> parseProperties(String tag) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		Matcher matcherProperties = TAG_PROPERTIES_PATTERN.matcher(tag);
		while (matcherProperties.find()) {// 查找属性
			String property = matcherProperties.group();

			Matcher matcherPropertyKey = TAG_PROPERTIES_KEY_PATTERN.matcher(property);
			String key = null;
			if (matcherPropertyKey.find()) {
				key = matcherPropertyKey.group();
			}

			Matcher matcherPropertyvalue = TAG_PROPERTIES_VALUE_PATTERN.matcher(property);
			String value = null;
			if (matcherPropertyvalue.find()) {
				value = matcherPropertyvalue.group();
			}

			if (key != null && value != null) {
				map.put(key, value);
			}
		}
		return map;
	}
}
