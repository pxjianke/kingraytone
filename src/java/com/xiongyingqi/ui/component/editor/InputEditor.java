/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

import java.awt.Component;
import java.io.File;

import javax.swing.Icon;

import com.xiongyingqi.emotion.Emotion;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-4 下午3:01:41
 */
public interface InputEditor {
	/**
	 * 插入字符串类型
	 * <br>2013-9-4 下午3:14:33
	 * @param content
	 */
	public void insertString(String content);
	
	/**
	 * 插入图片类型
	 * <br>2013-9-4 下午3:14:42
	 * @param icon
	 */
	public void insertIcon(Icon icon);
	
	/**
	 * 插入图片，但是是传入的文件地址，推荐使用
	 * <br>2013-9-4 下午3:24:21
	 * @param iconFile
	 */
	public void insertIcon(File iconFile);
	
	/**
	 * 插入
	 * <br>2013-9-4 下午3:14:55
	 * @param content
	 */
	public void insertEmotion(Emotion emotionFile);
	
	/**
	 * 插入UI组件
	 * <br>2013-9-4 下午3:27:23
	 * @param component
	 */
	public void insertComponent(Component component);
}
