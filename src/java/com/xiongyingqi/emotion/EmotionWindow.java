/**
 * spark_src
 */
package com.xiongyingqi.emotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JWindow;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import com.xiongyingqi.ui.JSystemFileChooser;
import com.xiongyingqi.ui.component.ImageLabel;
import com.xiongyingqi.ui.component.PlayImageLabel;
import com.xiongyingqi.ui.component.editor.InputEditor;
import com.xiongyingqi.utils.EntityHelper;

import javax.swing.JLabel;
import java.awt.GridLayout;
import java.io.File;

import javax.swing.JButton;

import org.jivesoftware.Spark;
import org.jivesoftware.spark.util.WindowsFileSystemView;

/**
 * 显示表情窗口
 * 
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-7 下午2:55:58
 */
public class EmotionWindow extends JWindow implements EmotionListener, AWTEventListener {
	private static final Color COLOR_ORANGE = new Color(255, 189, 63, 255);
	private static final Color COLOR_ORANGE_LIGHTLY = new Color(255, 189, 63, 150);
	private static final Color COLOR_MOUSE_LEAVE = new Color(255, 255, 255, 255);
	private static final Color COLOR_ALPHA = new Color(255, 255, 255, 0);// 透明颜色
	/**
	 * 表情面板每行摆放表情数
	 */
	private final int colunmSize = 14;
	/**
	 * 最小行数
	 */
	private final int minRowSize = 9;
	
	
	private static final int LABEL_WIDTH = 32;
	
	private static final int LABEL_HEIGHT = 32;
	
	private static final int IMAGE_WIDTH = 24;
	
	private static final int IMAGE_HEIGHT = 24;
	
	
	private static final Object LOCK = new Object();
	private static EmotionWindow singleton;
	private EmotionManager emotionManager;
	/**
	 * 当前是否选择共享表情标志
	 */
	private boolean isPublic = true;
	/**
	 * 显示该窗口的当前被点击的EmotionToggleButton
	 */
	private EmotionToggleButton currentEmotionToggleButton;
	/**
	 * 当前正在输入的编辑框
	 */
	private InputEditor currentInputEditor;
	private JPanel emotionPanel;
	private JLabel titleLabel;
	private JButton addEmotionButton;
	private JLabel publicEmotionLabel;
	private JLabel privateEmotionLabel;

	private EmotionWindow() {
		emotionManager = EmotionManager.getInstance();
		emotionManager.addEmotionListener(this);
		
		getContentPane().setBackground(Color.WHITE);

		JPanel titlePanel = new JPanel();
		titlePanel.setBackground(Color.WHITE);
		getContentPane().add(titlePanel, BorderLayout.NORTH);

		titleLabel = new JLabel("共享表情");
		titlePanel.add(titleLabel);

		addEmotionButton = new JButton("添加");
		addEmotionButton.setToolTipText("添加共享表情，其他用户也可以使用该表情，注意：请勿添加色情、暴力等图片！");
		addEmotionButton.setBackground(Color.WHITE);
		titlePanel.add(addEmotionButton);
		addEmotionButton.addMouseListener(new MouseAdapter() {
			/**
			 * <br>
			 * 2013-9-9 上午11:29:50
			 * 
			 * @see java.awt.event.MouseAdapter#mouseEntered(java.awt.event.MouseEvent)
			 */
			public void mouseClicked(MouseEvent e) {
				JFileChooser fc = new JSystemFileChooser();
				fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				if (Spark.isWindows()) {
					fc.setFileSystemView(new WindowsFileSystemView());
				}
				fc.setDialogTitle("选择图片文件或文件夹");
				int returnVal = fc.showOpenDialog(null);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					emotionManager.addEmotion(file, "", !isPublic);
//					field.setText(file.getAbsolutePath());
				}
				emotionManager.loadAllEmotions();
				showEmotionWindow();
			}
		});

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		emotionPanel = new JPanel();
		emotionPanel.setBorder(null);
		emotionPanel.setBackground(Color.WHITE);
		scrollPane.setViewportView(emotionPanel);

		JPanel operatePanel = new JPanel();
		operatePanel.setBackground(Color.WHITE);
		getContentPane().add(operatePanel, BorderLayout.SOUTH);
		operatePanel.setLayout(new GridLayout(0, 8, 2, 0));

		TabPanel publicEmotion = new TabPanel();
		publicEmotion.setBackground(Color.WHITE);
		operatePanel.add(publicEmotion);
		publicEmotion.setSelected(true);
		showPublicEmotions();// 显示共享表情

		publicEmotionLabel = new JLabel("共享表情");
		publicEmotion.add(publicEmotionLabel);
		publicEmotionLabel.addMouseListener(new MouseAdapter() {
			/**
			 * <br>
			 * 2013-9-9 上午11:12:51
			 * 
			 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
			 */
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);
				isPublic = true;
				titleLabel.setText(publicEmotionLabel.getText());
				addEmotionButton.setToolTipText("添加共享表情，其他用户也可以使用该表情，注意：请勿添加色情、暴力等图片！");
				showPublicEmotions();
			}
		});

		TabPanel privateEmotion = new TabPanel();
		privateEmotion.setBackground(Color.WHITE);
		operatePanel.add(privateEmotion);

		privateEmotionLabel = new JLabel("个人表情");
		privateEmotion.add(privateEmotionLabel);
		privateEmotionLabel.addMouseListener(new MouseAdapter() {
			/**
			 * <br>
			 * 2013-9-9 上午11:12:51
			 * 
			 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
			 */
			public void mousePressed(MouseEvent e) {
				super.mousePressed(e);
				isPublic = false;
				titleLabel.setText(privateEmotionLabel.getText());
				addEmotionButton.setToolTipText("添加个人表情，表情文件将只在本地存储");
				showPrivateEmotions();
			}
		});
		// this.setAlwaysOnTop(true);

		this.setSize(460, 320);
	}

	public static EmotionWindow getInstance() {
		synchronized (LOCK) {
			if (singleton == null) {
				singleton = new EmotionWindow();
				Toolkit toolkit = Toolkit.getDefaultToolkit();
				toolkit.addAWTEventListener(singleton, AWTEvent.MOUSE_EVENT_MASK);
			}
		}
		return singleton;
	}

	/**
	 * 显示表情窗口 <br>
	 * 2013-9-9 上午9:51:29
	 */
	public void showEmotionWindow() {
		this.setVisible(true);
		if(isPublic){
			showPublicEmotions();
		} else {
			showPrivateEmotions();
		}
	}

	/**
	 * 在特定的位置显示窗口 <br>
	 * 2013-9-9 上午9:51:38
	 * 
	 * @param x
	 * @param y
	 * @param currentEmotionToggleButton
	 */
	public void showEmotionWindow(int x, int y, EmotionToggleButton currentEmotionToggleButton,
			InputEditor inputEditor) {
		this.setLocation(caculatePoint(x, y));
		this.currentEmotionToggleButton = currentEmotionToggleButton;
		this.currentInputEditor = inputEditor;
		showEmotionWindow();
	}

	private Point caculatePoint(int x, int y) {
		x = x - this.getWidth() / 2;
		y = y - this.getHeight();

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension dimension = toolkit.getScreenSize();// 获取屏幕的大小
		double screenWidth = dimension.getWidth();
		double screenHeight = dimension.getHeight();
		int rightScreenXLimit = (int) (screenWidth - this.getWidth());

		int downScreenYLimit = (int) (screenHeight - this.getHeight());

		if (x > rightScreenXLimit) {
			x = rightScreenXLimit;
		}

		if (y > downScreenYLimit) {
			y = downScreenYLimit;
		}

		if (x < 0) {
			x = 0;
		}
		if (y < 0) {
			y = 0;
		}
		Point point = new Point(x, y);
		return point;
	}

	public void hideEmotionWindow() {
		this.setVisible(false);
	}

	/**
	 * <br>
	 * 2013-9-7 下午3:06:39
	 * 
	 * @see com.xiongyingqi.emotion.EmotionListener#emotionAdded(com.xiongyingqi.emotion.Emotion)
	 */
	@Override
	public void emotionAdded(Emotion emotion) {
		
	}

	/**
	 * <br>
	 * 2013-9-7 下午3:06:39
	 * 
	 * @see com.xiongyingqi.emotion.EmotionListener#emotionUpdated(com.xiongyingqi.emotion.Emotion)
	 */
	@Override
	public void emotionUpdated(Emotion emotion) {
	}

	/**
	 * <br>
	 * 2013-9-7 下午3:06:39
	 * 
	 * @see com.xiongyingqi.emotion.EmotionListener#emotionDeleted(com.xiongyingqi.emotion.Emotion)
	 */
	@Override
	public void emotionDeleted(Emotion emotion) {
	}

	/**
	 * 显示私有表情 <br>
	 * 2013-9-9 上午10:59:17
	 */
	private void showPrivateEmotions() {
		Collection<Emotion> emotions = new ArrayList<Emotion>(emotionManager.getPrivatEmotions());
		showEmotions(emotions);
	}

	/**
	 * 显示共享表情 <br>
	 * 2013-9-9 上午10:59:03
	 */
	private void showPublicEmotions() {
		Collection<Emotion> emotions = new ArrayList<Emotion>(emotionManager.getPublicEmotions());
		showEmotions(emotions);
	}

	private void showEmotions(Collection<Emotion> emotions) {
		emotionPanel.removeAll();// 清除所有表情
		emotionPanel.setLayout(null);
		if (emotions == null) {
			return;
		}
		int size = emotions.size();
		int row = size / colunmSize + (size % colunmSize > 0 ? 1 : 0);
		
		if(row < minRowSize){
			row = minRowSize;
		}
		
		emotionPanel.setLayout(new GridLayout(row, colunmSize));
		for (Iterator iterator = emotions.iterator(); iterator.hasNext();) {
			Emotion emotion = (Emotion) iterator.next();
			PlayEmotionLabel emotionLabel = new PlayEmotionLabel(emotion);
			emotionPanel.add(emotionLabel);
		}
		if(size < minRowSize * colunmSize){
			int leastEmotionSize = minRowSize * colunmSize - size;
			for (int i = 0; i < leastEmotionSize; i++) {
				JLabel label = new PlayEmotionLabel(new File(""));
				label.setSize(LABEL_WIDTH, LABEL_HEIGHT);
				label.setBackground(Color.WHITE);
				emotionPanel.add(label);
			}
		}
		
		emotionPanel.validate();
	}

	private boolean isPointInComponent(int x, int y) {
		Point point = this.getLocation();
		Dimension size = this.getSize();
		double startX = point.getX();
		double startY = point.getY();
		double endX = startX + size.getWidth();
		double endy = startY + size.getHeight();
		if (x >= startX && x <= endX && y >= startY && y <= endy) {
			return true;
		}
		return false;
	}

	/**
	 * <br>
	 * 2013-9-8 下午8:17:27
	 * 
	 * @see java.awt.event.AWTEventListener#eventDispatched(java.awt.AWTEvent)
	 */
	@Override
	public void eventDispatched(AWTEvent event) {
		if (event instanceof MouseEvent) {
			if (currentEmotionToggleButton != null) {
				MouseEvent mouseEvent = (MouseEvent) event;
				Object source = mouseEvent.getSource();
				if (mouseEvent.getID() == MouseEvent.MOUSE_PRESSED
						&& !((source instanceof EmotionToggleButton) || isPointInComponent(
								mouseEvent.getXOnScreen(), mouseEvent.getYOnScreen()))) {
					currentEmotionToggleButton.doMouseExited();
					this.hideEmotionWindow();
				} else if (mouseEvent.getID() == MouseEvent.MOUSE_EXITED
						&& !(mouseEvent.getSource() instanceof EmotionToggleButton)) {

				}
			}
		}
	}

	class PlayEmotionLabel extends ImageLabel {
		private Icon scaledIcon;
		private Emotion emotion;
		/**
		 * @param iconFile
		 */
		public PlayEmotionLabel(File iconFile) {
			super(iconFile);
			init();
		}

		/**
		 * @param icon
		 */
		public PlayEmotionLabel(Icon icon) {
			super(icon);
			init();
		}
		
		public PlayEmotionLabel(Emotion emotion){
			super(emotion.getEmotionFile());
			this.emotion = emotion;
			init();
		}

		private void getScaledImage(){
			Image image = ( (ImageIcon) icon).getImage();
			if(icon.getIconHeight() > IMAGE_WIDTH || icon.getIconWidth() > IMAGE_HEIGHT){// 如果表情图片大于24，则创建缩放图
				image = image.getScaledInstance(24, 24, Image.SCALE_SMOOTH);
				scaledIcon = new ImageIcon(image);
				
//				image = ImageHelper.scaleImage(image, 24, 24, false);
//				scaledIcon = new ImageIcon(image);
//				if(FileHelper.getMimeType(iconFile).endsWith("gif")){
//					GifImage gifImage = null;
//					try {
//						EntityHelper.print(emotion.getScaledEmotionFile());
//						gifImage = GifDecoder.decode(emotion.getEmotionFile());
//						gifImage = GifTransformer.scale(gifImage, 24, 24, false);
//						GifEncoder.encode(gifImage, emotion.getScaledEmotionFile());
//						icon = new ImageIcon(FileHelper.toURL(emotion.getScaledEmotionFile()));
//					} catch (IOException e1) {
//						e1.printStackTrace();
//					}
//					
//				} else {
//					image = image.getScaledInstance(24, 24, Image.SCALE_SMOOTH);
//					scaledIcon = new ImageIcon(image);
//				}
				
				this.setIcon(scaledIcon);
			}
		}
		private void init() {
			this.setBackground(COLOR_MOUSE_LEAVE);
			getScaledImage();
			this.addMouseListener(new MouseAdapter() {
				/**
				 * <br>2013-9-9 下午1:11:36
				 * @see java.awt.event.MouseAdapter#mouseEntered(java.awt.event.MouseEvent)
				 */
				@Override
				public void mouseEntered(MouseEvent e) {
//					PlayEmotionLabel.this.setBackground(COLOR_ORANGE_LIGHTLY);
					PlayEmotionLabel.this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
//					PlayEmotionLabel.this.setBorder(BorderFactory.createEtchedBorder(COLOR_ORANGE_LIGHTLY, COLOR_ORANGE));
//					PlayEmotionLabel.this.setIcon(icon);
//					PlayEmotionLabel.this.invalidate();
				}
				
				/**
				 * <br>2013-9-9 下午1:12:44
				 * @see java.awt.event.MouseAdapter#mouseExited(java.awt.event.MouseEvent)
				 */
				@Override
				public void mouseExited(MouseEvent e) {
//					PlayEmotionLabel.this.setBackground(COLOR_MOUSE_LEAVE);
					PlayEmotionLabel.this.setBorder(BorderFactory.createLineBorder(COLOR_ORANGE));
//					PlayEmotionLabel.this.setIcon(scaledIcon);
//					PlayEmotionLabel.this.invalidate();
				}
				/**
				 * <br>2013-9-9 下午1:24:13
				 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
				 */
				@Override
				public void mousePressed(MouseEvent e) {
					if(PlayEmotionLabel.this.emotion != null){
						EmotionWindow.this.currentInputEditor.insertEmotion(PlayEmotionLabel.this.emotion);
					}
				}
			});
			this.setSize(LABEL_WIDTH, LABEL_HEIGHT);
			this.setBorder(BorderFactory.createLineBorder(COLOR_ORANGE));
		}
		
		
		
	}

	static class TabPanel extends JPanel implements MouseListener {
		private static Collection<TabPanel> tabPanels = new ArrayList<EmotionWindow.TabPanel>();
		private boolean isSelected;

		/**
		 * boolean
		 * 
		 * @return the isSelected
		 */
		public boolean isSelected() {
			return isSelected;
		}

		/**
		 * <br>
		 * 2013-9-9 上午11:18:39
		 * 
		 * @see java.awt.Container#add(java.awt.Component)
		 */
		@Override
		public Component add(Component comp) {
			comp.addMouseListener(this);
			return super.add(comp);
		}

		/**
		 * boolean
		 * 
		 * @param isSelected
		 *            the isSelected to set
		 */
		public void setSelected(boolean isSelected) {
			this.isSelected = isSelected;
			if (!isSelected) {
				this.setBackground(COLOR_MOUSE_LEAVE);
			} else {
				this.setBackground(COLOR_ORANGE);
			}
		}

		public TabPanel() {
			tabPanels.add(this);
			init();
		}

		/**
		 * <br>
		 * 2013-9-9 上午11:01:20
		 */
		private void init() {
			setBackground(COLOR_MOUSE_LEAVE);
			this.addMouseListener(this);
		}

		/**
		 * <br>
		 * 2013-9-9 上午11:03:27
		 * 
		 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
		 */
		@Override
		public void mouseClicked(MouseEvent e) {
		}

		/**
		 * <br>
		 * 2013-9-9 上午11:03:27
		 * 
		 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
		 */
		@Override
		public void mousePressed(MouseEvent e) {
			for (Iterator iterator = tabPanels.iterator(); iterator.hasNext();) {
				TabPanel tabPanel = (TabPanel) iterator.next();
				tabPanel.setSelected(false);
			}
			setSelected(true);
		}

		/**
		 * <br>
		 * 2013-9-9 上午11:03:27
		 * 
		 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
		 */
		@Override
		public void mouseReleased(MouseEvent e) {
			
		}

		/**
		 * <br>
		 * 2013-9-9 上午11:03:27
		 * 
		 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
		 */
		@Override
		public void mouseEntered(MouseEvent e) {
			if (!isSelected) {
				this.setBackground(COLOR_ORANGE);
			}
		}

		/**
		 * <br>
		 * 2013-9-9 上午11:03:27
		 * 
		 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
		 */
		@Override
		public void mouseExited(MouseEvent e) {
			if (!isSelected) {
				this.setBackground(COLOR_MOUSE_LEAVE);
			}
		}

	}

	public static void main(String[] args) {
		EmotionWindow.getInstance().showEmotionWindow();
	}

}
