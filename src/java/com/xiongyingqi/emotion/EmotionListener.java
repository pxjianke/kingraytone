/**
 * spark_src
 */
package com.xiongyingqi.emotion;

/**
 * 表情对象监听<br>
 * 如果要捕获表情对象的行为，请使用EmotionManager.getInstance.addEmotionListener(EmotionListener);<br>
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-7 下午2:57:09
 */
public interface EmotionListener {
	/**
	 * 当客户端有新表情对象新增时通知该方法
	 * <br>2013-9-7 下午2:58:21
	 * @param emotion
	 */
	public void emotionAdded(Emotion emotion);
	/**
	 * 当客户端有表情对象更新时通知该方法
	 * <br>2013-9-7 下午2:59:55
	 * @param emotion
	 */
	public void emotionUpdated(Emotion emotion);
	
	/**
	 * 当客户端删除表情对象时通知该方法
	 * <br>2013-9-7 下午3:02:03
	 * @param emotion
	 */
	public void emotionDeleted(Emotion emotion);
}
