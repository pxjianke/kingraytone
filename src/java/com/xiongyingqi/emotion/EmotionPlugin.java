/**
 * spark_src
 */
package com.xiongyingqi.emotion;


import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.jivesoftware.resource.SparkRes;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.spark.ChatManager;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.spark.component.ToggleButton;
import org.jivesoftware.spark.plugin.Plugin;
import org.jivesoftware.spark.ui.ChatRoom;
import org.jivesoftware.spark.ui.ChatRoomListener;

import com.kingray.spark.packet.KingrayNameSpace;
import com.xiongyingqi.ui.component.PlayImageLabel;


/**
 * 图标服务类
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-5 上午11:08:07
 */
public class EmotionPlugin implements Plugin, ChatRoomListener{
	
	private EmotionManager emotionManager;
	private ChatManager chatManager;
	private EmotionService emotionService;
	
	/**
	 * <br>2013-9-5 上午11:22:41
	 * @see org.jivesoftware.spark.plugin.Plugin#initialize()
	 */
	@Override
	public void initialize() {
		ProviderManager.getInstance().addIQProvider(KingrayNameSpace.QUERY_EMOTION.getKey(), KingrayNameSpace.QUERY_EMOTION.getValue(), new EmotionIQProvider());
		emotionManager = EmotionManager.getInstance();
		chatManager = SparkManager.getChatManager();
		chatManager.addChatRoomListener(this);
		
		emotionService = EmotionService.getInstance();
		
		SparkManager.getConnection().addPacketListener(emotionService, emotionService);
		
		emotionManager.synchonizeEmotionsInthread();// 同步表情
	}

	/**
	 * <br>2013-9-5 上午11:22:41
	 * @see org.jivesoftware.spark.plugin.Plugin#shutdown()
	 */
	@Override
	public void shutdown() {
	}

	/**
	 * <br>2013-9-5 上午11:22:41
	 * @see org.jivesoftware.spark.plugin.Plugin#canShutDown()
	 */
	@Override
	public boolean canShutDown() {
		return false;
	}

	/**
	 * <br>2013-9-5 上午11:22:41
	 * @see org.jivesoftware.spark.plugin.Plugin#uninstall()
	 */
	@Override
	public void uninstall() {
		ProviderManager.getInstance().removeIQProvider(KingrayNameSpace.QUERY_EMOTION.getKey(), KingrayNameSpace.QUERY_EMOTION.getValue());
	}

	/**
	 * <br>2013-9-5 上午11:25:28
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#chatRoomOpened(org.jivesoftware.spark.ui.ChatRoom)
	 */
	@Override
	public void chatRoomOpened(final ChatRoom room) {
		final EmotionToggleButton emotionButton = new EmotionToggleButton(SparkRes.getImageIcon(SparkRes.EMOTION_ICON));
		room.addEditorComponent(emotionButton);
		emotionButton.addMouseListener(new MouseAdapter() {
			/**
			 * <br>2013-9-8 下午9:39:28
			 * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
			 */
			@Override
			public void mousePressed(MouseEvent e) {
				if(!emotionButton.isMouseDown()){
					EmotionWindow.getInstance().hideEmotionWindow();
					emotionButton.setMouseDown(false);
				} else {
					EmotionWindow.getInstance().showEmotionWindow((int) emotionButton.getLocationOnScreen().getX(), (int) emotionButton.getLocationOnScreen().getY(), emotionButton, room.getChatInputEditor());
					emotionButton.setMouseDown(true);
				}
				super.mousePressed(e);
			}
		});
	}

	/**
	 * <br>2013-9-5 上午11:25:28
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#chatRoomLeft(org.jivesoftware.spark.ui.ChatRoom)
	 */
	@Override
	public void chatRoomLeft(ChatRoom room) {
		EmotionWindow.getInstance().hideEmotionWindow();
	}

	/**
	 * <br>2013-9-5 上午11:25:28
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#chatRoomClosed(org.jivesoftware.spark.ui.ChatRoom)
	 */
	@Override
	public void chatRoomClosed(ChatRoom room) {
		EmotionWindow.getInstance().hideEmotionWindow();
	}

	/**
	 * <br>2013-9-5 上午11:25:28
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#chatRoomActivated(org.jivesoftware.spark.ui.ChatRoom)
	 */
	@Override
	public void chatRoomActivated(ChatRoom room) {
	}

	/**
	 * <br>2013-9-5 上午11:25:28
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#userHasJoined(org.jivesoftware.spark.ui.ChatRoom, java.lang.String)
	 */
	@Override
	public void userHasJoined(ChatRoom room, String userid) {
	}

	/**
	 * <br>2013-9-5 上午11:25:28
	 * @see org.jivesoftware.spark.ui.ChatRoomListener#userHasLeft(org.jivesoftware.spark.ui.ChatRoom, java.lang.String)
	 */
	@Override
	public void userHasLeft(ChatRoom room, String userid) {
	}

	public static void main(String[] args) {
		SparkRes.getImageIcon("emotion.png");
	}
}
